Gallery 
========================  

# Photos  

## Image  

To update the photos in `img/gallery/photos` folder, please include 

1. Resized image with a max width of `1024` for landscape image, or a max height of `1024` for portrait image.
2. Resized thumbnail image with a height of `200`, with adding `thumb-` infront of the naming of the image (i.e. `thumb-img-ilce-9-2`).
3. The gallery image naming format will be `img`  + `model` + `photo ID` (i.e. for the second photo of a9 camera, the photo ID you were given is `2`, so the outcome will be `img-ilce-9-2`).


## Data  

1. Data is located at `data/photos.json`.  
2. Way of filling the data
	* id : increasing as per every new additional photo
	* url : image path of photo
	* thumb
		- src : image path of thumbnail
		- width : may differ with different ratio of the photo
		- height : fixed height of 200
	* camera : model in caps
	* lens : lens model in caps
	* categories : category of the photo, can have more than 1
	* caption : specs of the photo


~~~~
	{
		"id": 2,
		"url": "img/gallery/photos/img-ilce-7rm2-2.jpg",
		"thumb": {
			"src": "img/gallery/photos/thumb-img-ilce-7rm2-2.jpg",
			"width": 300,
			"height": 200
		},
		"camera": "ILCE-7RM2",
		"lens": "SEL2470GM",
		"categories": [ "sportsaction" ],
		"caption": "a7R II | FE 24-70mm F2.8 GM | 1/2.5 sec. | F11.0 | ISO200"
	}
~~~~



## To Add New Models at gallery.template.html

~~~~
	<select id="filter-model" class="select-model filter-border" style="width:100%;">
		<option value="" selected>All Cameras</option>
		<!-- Add New Model Here --> 
	</select>
~~~~

~~~~
	<select id="filter-model-mobile" class="select-model filter-border">
		<option value="" selected>All Camera</option>
		<!-- Add New Model Here --> 
		</option>
	</select>
~~~~

## To Add New Lenses at gallery.template.html
	
~~~~
	<select id="filter-lenses" class="select-lenses filter-border" style="width:100%;">
		<option value="" selected>All Lenses</option>
		<!--Add New Lenses Here -->
	</select>
~~~~

~~~~
	<select id="filter-lenses-mobile" class="select-lenses filter-border" style="width:100%;">
		 <option value="" selected>All Lenses</option>
		 <!--Add New Lenses Here -->
	</select>
~~~~  



- - - -



# Videos  

1. Data is located at `data/videos.json`. To update the videos, please retrieve the youtubeid from the query string v.  
2. Way of filling the data
	* id : increasing as per every new additional video
	* youtubeid : retrieve the youtubeid from the query string v
	* bodyModel : model in caps (if there's any)
	* typeName : lens type (if there's any)
	* caption : caption of the video, mostly copy from youtube title

~~~~
	{
		"id": 1,
		"youtubeid": "SSkfW1A01bE",
		"bodyModel": "",
		"typeName": "g-master",
		"caption": "G Master – Brand Concept"
	}
~~~~


## To Add New Body at gallery.template.html

~~~~
	<select id="filter-body" class="select-body filter-border" style="width:100%;">
	    <option value="" selected>All Bodies</option>
	    <!--Add New Body Here -->
	</select>
~~~~

~~~~
	<select id="filter-body-mobile" class="select-body filter-border">
	    <option value="" selected>All Bodies</option>
	    <!--Add New Body Here -->
	</select>
~~~~  

## To Add New Type at gallery.template.html

~~~~
	<select id="filter-type" class="select-type filter-border" style="width:100%;">
	    <option value="" selected>All Types</option>
	    <!--Add New Type Here -->
	</select>
~~~~

~~~~
	<select id="filter-type-mobile" class="select-type filter-border">
		<option value="" selected>All Types</option>
		<!--Add New Type Here -->
	</select>
~~~~

