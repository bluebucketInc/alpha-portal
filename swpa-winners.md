SWPA 
========================  


# Landing Page  

* `swpa/index.template.html` to edit landing page html structure.
* `less/swpa.less` to edit CSS.
*  `js/swpa.js` to edit JS. 


- - - -


# SWPA Winners Page


* `swpa/winners.template.html` to edit SWPA winners html structure.  
* `less/swpa-winners.less` to edit CSS.  
* `js/swpa-winners.js` to edit JS.  
 


## Image  

To update the photos in `img/swpa/photos` folder, please include 

1. Resized image with a max width of `1024` for landscape image, or a max height of `1024` for portrait image.
2. Resized thumbnail image with a height of `200`, with adding `thumb-` in front of the naming of the image (i.e. `thumb-img-wong-wai-sun-1`).
3. The SWPA winners image naming format will be `img`  + `photographer name` + `photo ID` (i.e. for the second photo of alex, the photo ID you were given is `2`, so the outcome will be `img-alex-2`).  


## Data

1. Data is located at `data/swpa.json`.
2. Way of filling the data
	* id : increasing as per every new additional photo
	* url : image path of photo
	* thumb
		- src : image path of thumbnail
		- width : may differ with different ratio of the photo
		- height : fixed height of 200
	* title : Title of the photo
	* copyright : "© Wong Wai Sun, Malaysia, 2nd Place, National Awards, 2017 Sony World Photography Awards"
	* year : "2017"
	* competition : "national" / "open" / "professional" / "youth"
	* country : Country code
	* categories : category of the photo, can have more than 1
	* caption : Description of the photo

~~~~
	{
		"id": 110,
		"url": "img/swpa/photos/img-wong-wai-sun-1.jpg",
		"thumb": {
			"src": "img/swpa/photos/thumb-img-wong-wai-sun-1.jpg",
			"width": 280,
			"height": 200
		},
        "title": "Relax",
		"copyright": "© Wong Wai Sun, Malaysia, 2nd Place, National Awards, 2017 Sony World Photography Awards",
		"year": 2017,
        "competition": "national",
        "country": "my",
		"categories": [ "culture", "people" ],
		"caption": "Old men playing cards."
	}
~~~~


## To Add New Competitions at swpa/winners.template.html

~~~~
	<select id="filter-competition" class="select-competition filter-border" style="width:100%;">
		<option value="" selected>All Competitions</option>
		<!-- Add New Competitions Here --> 
	</select>
~~~~

~~~~
	<select id="filter-competition-mobile" class="select-competition filter-border">
        <option value="" selected>All Competitions</option>
        <!-- Add New Competitions Here --> 
    </select>
~~~~


## To Add New Country at swpa/winners.template.html  

Adding a new country may refer to this [Site][] for the country code.

[Site]: https://www.freeformatter.com/iso-country-list-html-select.html

~~~~
	<select id="filter-country" class="select-country filter-border" style="width:100%;">
        <option value="" selected>All Countries</option>
		<!-- Add New Country Here --> 
	</select>
~~~~

~~~~
	<select id="filter-country-mobile" class="select-country filter-border" style="width:100%;">
        <option value="" selected>All Countries</option>
        <!-- Add New Country Here --> 
    </select>
~~~~  

## To Add New Categories at swpa/winners.template.html  

~~~~
	 <div class="swpa-categories-container">
        <div class="filter-btn text-center" data-categories="art">
            <div class="wrapper">
                <p>Art &amp; Culture</p>
            </div>
        </div>
        <!-- Add New Categories Here --> 
    </div>
~~~~  

~~~~
	<select id="filter-swpa-categories-mobile" class="select-swpa-categories filter-border">
        <option value="" selected>All Categories</option>
        <!-- Add New Categories Here --> 
    </select>
~~~~ 
