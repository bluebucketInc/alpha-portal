{
    "What's a camera": [
        {
            "name": "Viewfinder Technologies: EVF and OVF",
            "excerpt": "A viewfinder is any device that allows you to see and frame what you are shooting. Traditionally, the viewfinder refers to the little window near the top of a camera’s body that allows the photographer to"
        },
        {
            "name": "Image Stabilisation Technologies",
            "excerpt": "Blurry photos are the bane of all photographers. They are caused by movement of the camera during exposure, such as the shaking of the photographer’s own hands; exacerbated by shooting"
        },
        {
            "name": "Digital Image Formats",
            "excerpt": "Today, there are dozens of digital image formats available. If you’ve been using a computer for any amount of time, you’re bound to have come across many; the most common being BMP, GIF"
        },
        {
            "name": "Mirrorless System Cameras",
            "excerpt": "A Mirrorless system camera is a class of digital camera that is developed as an alternative to DSLRs, often touting comparable image quality with the same sensor and image processors found in a"
        },
        {
            "name": "Sensor Sizes",
            "excerpt": "Digital sensor sizes and their terminology have their roots in film. In the past, photographic film came in a few standard sizes – the most common being the 35mm standard – so named because of its width"
        },
        {
            "name": "Image Capture: Sensor Technologies",
            "excerpt": "In any camera, something is needed to capture the light passing through the lens to create the image. In the past, it was photographic film; but in a digital camera, this is now done by a device known"
        },
        {
            "name": "Image Capture: The Microprocessor",
            "excerpt": "Now that you know how digital camera sensors collects light that passes through its lens during exposure, what happens next is the conversion of this light data into the final output you see as a digital"
        },
        {
            "name": "Optical Low Pass Filter and Moiré",
            "excerpt": "Digital cameras sometimes produce an optical error known as an aliasing or moiré effect when shooting subjects with repeating patterns or lines. The moiré effect can appear as jagged lines, blended lines"
        },
        {
            "name": "Crop Factor",
            "excerpt": "Take a lens and put it on two different DSLR cameras. They should both give you the same view, right? But they don’t always due to something called the crop factor"
        },
        {
            "name": "Focal Length",
            "excerpt": "The focal length of a lens is the distance from its center to the film or sensor of a camera and is measured in millimeters. A longer focal length means it has a longer zoom range, so an 85mm lens will let you"
        },
        {
            "name": "About Phase, Contrast and Hybrid Autofocus",
            "excerpt": "Most digital cameras today rely on one of two methods to achieve auto-focus: Phase detection autofocus or contrast detection autofocus. Some cameras have also started to use a hybrid combination"
        }
    ],
    "How to use your camera": [
        {
            "name": "White Balance",
            "excerpt": "Have you ever shot photographs indoors which turned out way too yellow or too blue? That happens because a camera’s image sensor can be confused into misinterpreting"
        },
        {
            "name": "Making Sense of MTF Charts",
            "excerpt": "An MTF chart is a tool that allows photographers to judge the performance of a lens in terms of its ability to generate contrast and resolution. Most lens makers provide an MTF chart in the brochures"
        },
        {
            "name": "Caring for Your Cameras & Lenses",
            "excerpt": "A good digital camera and set of lenses can bring you many years, even decades of service if taken care of properly. To ensure your equipment is in tip-top condition, here are basic care and storage"
        },
        {
            "name": "Exposure Reading Histograms",
            "excerpt": "Image histograms are graphical representations of the pixels that compose any digital image and their distribution according to tonal variation"
        },
        {
            "name": "AE, FE & AF Lock",
            "excerpt": "AE Lock (AEL) stands for Auto Exposure Lock. Most advanced compact, DSLR and mirrorless camera models have a dedicated button to activate AE lock"
        },
        {
            "name": "Mode Dial Explained",
            "excerpt": "If you want to master your camera, the first thing you need to do after turning it on is to switch away from Auto. The path to photographic expression starts with the mode dial, where you decide how"
        },
        {
            "name": "Flash Sync",
            "excerpt": "If you’ve never ventured into your camera’s built-in flash settings, you might be surprised by the different results you can get just by changing a few things. Depending on your camera model, you might"
        },
        {
            "name": "Hyperfocal Distance & Diffraction",
            "excerpt": "One of the challenges of landscape photography is focusing so that everything in your photograph looks sharp. In order to do that, you have to understand the hyperfocal distance and the"
        },
        {
            "name": "ISO",
            "excerpt": "In traditional film photography, ISO refers to the measurement of light sensitivity of the film used in a camera. With the advent of digital photography, the term ISO is brought forward and continues to be used"
        },
        {
            "name": "Aperture",
            "excerpt": "Aperture refers to the variable opening on a lens which light passes through. The physical device that controls aperture—often called a diaphragm or an iris—is made up of a series of circular interlocking blades"
        },
        {
            "name": "Shutter Speed",
            "excerpt": "In a camera, digital or otherwise, the shutter is a mechanical device that sits between the lens and the sensor (or film). When you press the \"shutter release button\" to take a photo, the shutter opens to let light come"
        },
        {
            "name": "Depth of Field",
            "excerpt": "Depth of field (DOF) is a term that’s used to describe the distance in front of and behind the actual focus point in a picture that remains in focus. In photography, your lens really only has a single precise focal point"
        },
        {
            "name": "Metering Explained",
            "excerpt": "Every camera has a built-in light meter, and metering is the process of how your camera determines what the proper exposure should be for any scene. There are a few methods of doing this and most cameras"
        },
        {
            "name": "Autofocus Areas and Modes",
            "excerpt": "There are many options and techniques that can affect your final photo, from the creation of depth and background defocusing through aperture, to the illusion of speed through motion panning and slow"
        },
        {
            "name": "Sony α Photography Cheat Sheet",
            "excerpt": ""
        }
    ],
    "Before you shoot": [
        {
            "name": "Lighting with Flash: Basics",
            "excerpt": "Like it or not, there are times when available light just isn’t enough and you have to bring your own light. If you’ve never ventured off the flash which came built into your camera, you may hate the way flash"
        },
        {
            "name": "Lighting with Flash: Advanced",
            "excerpt": "Now that you’ve learned the basics of using flash for well-lit shots and the essential properties and features of flash units, you can venture into more creative lighting by bringing more lights into the picture"
        },
        {
            "name": "The Rule of Thirds",
            "excerpt": "Despite its name, the rule of thirds isn’t a rule at all, but a guiding principle to help us compose interesting images. Imagine dividing a frame into three equal parts with horizontal and vertical lines. The rule of"
        },
        {
            "name": "Color",
            "excerpt": "Your choice of color establishes emotion, and through the careful use of color you can conjure specific emotional responses from your audience. Color is an essential element of composition, here’s how to use it"
        },
        {
            "name": "Perspective",
            "excerpt": "Photography flattens three-dimensions into two, so how do we make it feel like our photos contain depth and space? Because the medium of photography is flat and two-dimensional, we need to"
        },
        {
            "name": "Framing",
            "excerpt": "Photography is the act of framing the world, one frozen moment at a time. So how do we arrange the frame itself? While we pay a lot of attention to the content within a photograph, we should pay as much"
        },
        {
            "name": "Telling a Story",
            "excerpt": "We all love a beautiful photograph, but photos which linger in our mind's eye are the ones that transcend being just a pretty picture. They do more than convey an aesthetically pleasing combination of line, form and"
        },
        {
            "name": "Exposure: The Stop Unit and EV Table",
            "excerpt": "Photography can be daunting to the uninitiated with all the crosstalk between aperture, shutter speed and ISO speed. True, they’re all independent functions on their own, but at the same time they"
        }
    ],
    "Taking Great Pictures": [
        {
            "name": "Portraits",
            "excerpt": "A portrait creates a relationship between two people; the subject and the viewer. How that relationship unfolds depends on you, the photographer. Portraits come in various types, ranging from outdoor to indoor"
        },
        {
            "name": "Wildlife",
            "excerpt": "Wildlife photograph is especially challenging because it can require you to get out of your comfort zone in several areas, least of all in your photographic technique"
        },
        {
            "name": "Action",
            "excerpt": "Photography is all about the decisive moment, and nothing tests your ability to capture the moment like shooting action. From racing cars that disappear in second, to a dog jumping for the ball, to a boxer’s face the"
        },
        {
            "name": "Low Light",
            "excerpt": "There’s something alluring about the night. When the sun sets and shadows come out to play, the world you see through the lens becomes entirely new. But with its scarcity of light, night photography"
        },
        {
            "name": "Landscape",
            "excerpt": "Landscape photography is about the spirit of spaces. The best landscapes don’t just show you what a place looks like; they reveal what a place feels like. Landscapes are often images of the natural world with a"
        },
        {
            "name": "Macro",
            "excerpt": "Macro photography is the art of shooting objects up close, making them stunningly larger than life. With a keen eye, even the smallest of things can become the most interesting of images"
        },
        {
            "name": "Architecture",
            "excerpt": "Nestled behind the idea of a building are leading lines, complementary shapes, contrasting colors, rich textures and plays of light and shadow just waiting to be discovered. Photographing"
        },
        {
            "name": "Shooting for Stock",
            "excerpt": "Say you’ve been shooting for some time, and you’ve become quite adept at it. Your images have reached a level that comes close to a professional photographer’s and you ask yourself: What now? You could"
        },
        {
            "name": "Black and White",
            "excerpt": "Nothing is as essential as a black and white photograph. In the absence of color, texture and line gain immediacy, a photograph becomes pure subject free from distraction. Shooting for black and white"
        },
        {
            "name": "Travel",
            "excerpt": "It’s never been easier to travel, but what about photographing your travels? Many travelers return from their trips with little more than memories of missed photos, and you don’t want that to happen to you"
        },
        {
            "name": "Street",
            "excerpt": "Henri Cartier-Bresson once said that “there is nothing in this world that does not have a decisive moment.” Street photography is, in essence, the capture of those honest moments. Unlike shooting a"
        },
        {
            "name": "Landscapes & Filters",
            "excerpt": "Filters help you get the photograph right in the camera. Despite how far digital capture and post-processing has become, there are still effects that can’t be faked and exposures which can’t be fixed"
        }
    ],
    "Touching Up for Effect": [
        {
            "name": "Working with RAW",
            "excerpt": "Using Sony’s Image Data Converter (IDC) software to sort, rate and edit raw files shot from a Sony camera"
        },
        {
            "name": "Photo Printing and Resolution",
            "excerpt": "Digital image resolution – All digital photos are composed of tiny dots called “pixels”. In general, resolution is the expression of its size in terms of pixels. For any digital display, resolution is usually stated as"
        }
    ]
}