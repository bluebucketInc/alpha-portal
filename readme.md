Gulp Usage 
========================

## How To Use ##  

Ensure you have node and npm installed , then run 

`npm install`
`bower install`

Once the node modules are installed, you are ready to go.



# Development

## Compilation  

Note for Image folder:  

Running Gulp will not generate `img` folder within `dist` folder as the file size would be too huge. For development purpose, `img` folder needs to be manually added to `dist` folder and later on removed when development is done, only commmit the `img` folder outside the `dist` folder.  


### Local

Run `gulp` for development environment and `gulp build` for production purposes.

### Staging

Run `--staging` as a parameter to compile the codes (i.e. `gulp build --staging`). Use for staging.

### Production / Live

Run `--production` as a parameter to compile the codes (i.e. `gulp build --production`). Use for production.


## Styling

All styling are to be done in the `less` folder in `.less` format.

Except for iframe styling are to be done in the `css` folder in `.css` format.



Ambassadors page
================

There are 2 versions of ambassadors page now.
Version 1 - Opening pop up (the current working file is `ambassadors.template.html`)
Version 2 - Opening separate ambassador page (the current working fiel is `ambassadors2.template.html`)

Running `gulp` or `gulp build` will generate `ambassadors.html` and `ambassadors2.html` respectively. From the `Ambassadors` tab in the site, it will only link to `ambassadors.html`

As such, if Version 2 is to be used, simply change `ambassadors2.template.html` to `ambassadors.template.html` and the existing `ambassadors.template.html` to `ambassadors2.template.html`, then run `gulp` for development environment and `gulp build` for production. 


## Tracking Code  

There are 3 types of tracking code, please take note whenever you creating a new page or adding a new link.

1. ***omniture tracking*** - place at the bottom of every page.  
2. ***cid*** - to be added behind a URL
	- (i.e. https://www.worldphoto.org/sony-world-photography-awards`?cid=dealers:alpha_professionals_sg:redirect`)  
3. ***onclick*** - to be added inside an anchor link
	- (i.e. <a href="//sonygroup.box.com/s/lj3rfes87rip8vju9njyg2mz89qjfapk" target="_blank" `onclick="var e = arguments[0] || window.event; if (e.stopPropagation != null) e.stopPropagation(); else e.cancelBubble = true; trackMs_link('ms:ilc:@@country:a7Series:cameras:brochure-download', 'ilc:@@country', 'ilc:@@country:a7Series:cameras:brochure-download', 'microsite|ilc:@@country|a7Series|cameras|brochure-download')`;"></a>)  




