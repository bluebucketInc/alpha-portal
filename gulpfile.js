var gulp = require('gulp');
var browserSync = require('browser-sync');
var plugins = require('gulp-load-plugins')();
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var minifyCSS = require('gulp-minify-css');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');
var less = require('gulp-less');
var util = require('gulp-util');
var replace = require('gulp-replace');
var fileinclude = require('gulp-file-include');
var debug = require('gulp-debug');
var rename = require('gulp-rename');
var merge = require('merge-stream');

var config = {
    'production': !!util.env.production,
    'staging': !!util.env.staging,
    'port': 3000,
    'domain': 'http://localhost:<port>/'
};

if (config.production) {
    config.domain = 'http://www.sony-asia.com/microsite/aportal/';
} else if (config.staging) {
    config.domain = 'http://aportal.edmrsvp.com/aportal/2015/';
} else {
    config.domain = config.domain.replace('<port>', config.port);
}

gulp.task('browserSync', function() {
    browserSync({
        server: {
            baseDir: './dist'
        },
        port: config.port
    });
});

gulp.task('watch', ['browserSync'], function() {
    // Reloads the browser whenever HTML or JS files change
    gulp.watch('./less/**/*.less', ['css-watch']);
    gulp.watch('./css/**/*.css', browserSync.reload);
    gulp.watch(['./**/*.template.html', 'template/**/*.html', './js/**/*.js'], ['html-watch']);
    gulp.watch(['data/**/*.json'], ['json']);
});

gulp.task('css-watch', ['tmpassets'], browserSync.reload);

gulp.task('html-watch', ['useref'], browserSync.reload);

gulp.task('useref', ['template', 'tmpassets'] , function() {
    if (config.production || config.staging) {
        var assets = useref.assets();

        return gulp.src('.tmp/' + '**/*.template.html')
               .pipe(replace('http://localhost:3000/', config.domain))
               .pipe(rename({
                    extname: ''
               }))
               .pipe(rename({
                    extname: '.html'
               }))
               .pipe(assets)
               .pipe(gulpIf('**/*.css', minifyCSS()))
               .pipe(gulpIf('**/*.js', uglify()))
               .pipe(assets.restore())
               .pipe(useref())
               .pipe(gulp.dest('dist'));
    } else {
        return gulp.src('.tmp/' + '**/*.template.html')
               .pipe(replace('http://localhost:3000/', config.domain))
               .pipe(rename({
                    extname: ''
               }))
               .pipe(rename({
                    extname: '.html'
               }))
               .pipe(gulp.dest('dist'));
    }
});

gulp.task('template', function() {

    var files = ['**/*.template.html', '!.tmp/**/*'];

    return gulp.src(files)
        .pipe(fileinclude())
        .pipe(gulp.dest('.tmp/'));
});

gulp.task('tmpassets', ['less'], function() {
    if (config.production || config.staging) {
        return gulp.src([
                './js/**/*',
                './css/**/*',
                './bower_components/**/*',
                '!.tmp/**/*'
                ], {
                    base: '.'
                })
               .pipe(gulp.dest('.tmp/'));
    } else {
        return gulp.src([
                './js/**/*',
                './css/**/*',
                './bower_components/**/*',
                '!.tmp/**/*'
                ], {
                    base: '.'
                })
               .pipe(gulp.dest('dist'));
    }
});

gulp.task('less', function() {
    gulp.src(['less/styles.less'])
        .pipe(less())
        .pipe(gulp.dest('css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('json', function() {
    gulp.src('data/**/*.json')
        .pipe(plugins.jsonminify())
        .pipe(gulp.dest('dist/data/'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('images', function() {
    return gulp.src('./img/**/*.+(png|jpg|gif|svg|ico)')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'));
});

// copy files
gulp.task('copy', function() {
    return gulp.src([
            './fonts/**/*',
            './data/**/*',
            './css/images/**/*'
        ], {
            base: '.'
        })
        .pipe(gulp.dest('dist'));
});

gulp.task('clean', function(callback) {
    del.sync(['.tmp/', 'dist/**', '!dist', '!dist/img/**']);
    return cache.clearAll(callback);
});

gulp.task('build', function(callback) {
    runSequence('clean', 'slickAssets', 'less', ['useref', 'copy'],
    // runSequence('clean', 'slickAssets', 'less', ['useref', 'images', 'copy'],
        callback
    );
});

gulp.task('default', function(callback) {
    runSequence('build', ['browserSync', 'watch'],
        callback
    );
});

/* ----- ----- ----- ----- ----- */
/* slick assets */
/* ----- ----- ----- ----- ----- */
gulp.task('slickAssets', function() {
    // return merge(
    gulp.src(['bower_components/slick-carousel/slick/*.gif'])
        // .pipe(plugins.plumber({
        //     errorHandler: onError
        // }))
        // .pipe(plugins.changed('dist/css/'))
        .pipe(gulp.dest('dist/css/'));

    gulp.src('bower_components/slick-carousel/slick/fonts/*')
        // .pipe(plugins.plumber({
        //     errorHandler: onError
        // }))
        // .pipe(plugins.changed('dist/css/fonts/'))
        .pipe(gulp.dest('dist/css/fonts/'));
    // );
});
