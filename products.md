Products 
========================  

`product.template.html` to edit html structure.  

`less/product.less` to edit CSS.  

`js/product.js` to edit JS.  

`img/product/` to add or remove images.  

`data/products.json`to edit data.
   


# Adding New Camera/ Lenses


## Image

1. Images is located at `img/product/camera` for Camera, and `img/product/lens` for Lenses.
2. Product image naming to be follow model number of end Product URL. (i.e. for a9 camera, its URL is `http://www.sony-asia.com/electronics/interchangeable-lens-cameras/ilce-9`, so the image naming will be `ilce-9`).


## Data

1. Data is located at `data/products.json`.
2. Data for Camera is top half, and lenses is bottom half.
3. Way of filling the data
    * category : "camera" / "lens"
    * model : model number of end Product URL in caps format
    * mount : "a" / "e"
    * sensor : "fullframe" / "apsc"
    * title : name of camera / lens
    * subTitle : model number of end Product URL in text format
    * features : bullet points
    * url : URL of product
    * reviews : subject to avalability, read [Community Reviews][] to see how to add a new Community Reviews.
        - thumbnail : let it blank unless client request to add thumbnail for individual product(code twisting required at `product.template.html`), currently is linked masthead of article is thumbnial.
        - model : name of camera / lens
        - category : "cameras" / "lenses"
        - excerpt : first line of copy from article
    * testimonials : coming soon, just duplicate for the moment.
    * galleries : subject to avalability, only if the product has images in Gallery page.
        - thumbnail : let it blank unless client request to add thumbnail for individual product(code twisting required at `product.template.html`), currently all product are using the same thumbnail.
        - title : There's a default title currently, just duplicate.
        - url : partially product gallery images URL (i.e. `gallery.html?model=ILCE-6300` it will link to ***[http://aportal.edmrsvp.com/aportal/2015/gallery.html?model=ILCE-6300#photo](http://aportal.edmrsvp.com/aportal/2015/gallery.html?model=ILCE-6300#photo)***)


[Community Reviews]: https://bitbucket.org/interuptive/di-alpha-portal-revamp-2015/src/master/community-reviews.md

~~~~
    <!-- example Camera -->
        {
            "category": "camera",
            "model": "ILCE-6300-BODY-KIT",
            "mount": "e",
            "sensor": "apsc",
            "title": "α6300",
            "subTitle": "ILCE-6300 BODY / KIT",
            "features": [
                "The world’s fastest 0.051-sec. AF with the most phase-detection AF points (4252)",
                "24.2-megapixel Exmor&trade; CMOS image sensor/BIONZ X&trade; image processing engine",
                "4K movie recording with full pixel readout/without pixel binning enhances images"
            ],
            "url": "http://www.sony-asia.com/electronics/interchangeable-lens-cameras/ilce-6300-body-kit",
            "reviews": {
                "thumbnail": "",
                "model":"a6300",
                "category": "cameras",
                "excerpt": "The new a6300 with evolved “4D FOCUS” and more. The development team talks about the true potential hidden in its small body."
            },
            "testimonials": [
                {
                    "thumbnail": "",
                    "title": "",
                    "url": ""
                }
            ],
            "galleries": {
                "thumbnail": "",
                "title": "Uncover the most captivating photographs captured with the Sony α range of cameras",
                "url": "gallery.html?model=ILCE-6300"
            }
        }
    <!--  end of example Camera -->
~~~~



## To Add New Camera Mount at product.template.html  

~~~~
    <div class="filter-mount">
        <p class="filter-title">Camera Mount</p>
        <div class="filter-btn a-mount text-center" data-mount="a">
            <div class="wrapper">
                <p>A-mount</p>
            </div>
        </div>
        <!-- Add Camera Mount Here --> 
    </div>
~~~~

~~~~
    <div class="filter-mount">
        <p>Camera Mount</p>
        <select id="filter-mount">
            <option value="" selected disabled>Select</option>
            <!-- Add Camera Mount Here --> 
        </select>
    </div>
~~~~


## To Add New Sensor Size at product.template.html  

~~~~
    <div class="filter-sensor">
        <p class="filter-title">Sensor Size</p>
        <div class="filter-btn fullframe text-center" data-sensor="fullframe">
            <div class="wrapper">
                <p>Full Frame</p>
            </div>
        </div>
        <!-- Add New Sensor Size Here --> 
    </div>
~~~~  

~~~~
    <div class="filter-sensor">
        <p>Sensor Size</p>
        <select id="filter-sensor">
            <option value="" selected disabled>Select</option>
            <!-- Add New Sensor Size Here --> 
        </select>
    </div>
~~~~


## To Add New Type for Lenses at product.template.html

~~~~
    <div class="filter-type hidden">
        <p class="filter-title">Type</p>
        <div class="filter-btn g text-center" data-type="g_master">
            <div class="wrapper">
                <p>G Master Lenses</p>
            </div>
        </div>
        <!-- Add New Type Here --> 
    </div>
~~~~  

~~~~
    <div class="filter-type">
        <p>Type</p>
        <select id="filter-type">
            <option value="" selected disabled>Select</option>
            <!-- Add New Type Here --> 
        </select>
    </div>
~~~~