Shooting Techniques
========================  

To update the models on shooting techniques pages, locate the following section :  

~~~~
<div class="row models-row">
    <div class="col-sm-11" id="slider">
        <div class="models-slider">
            <div>
            	<!-- example product -->
                <div class="model-item" href="//www.sony-asia.com/electronics/camera-lenses/sel2870">
                    <div class="img-wrapper">
                        <div class="img-container center-block"><img src="img/product/lens/sel2870.jpg" width="220" height="152" alt="SEL2870" class="img-responsive"></div>
                    </div>
                     <p class="text-center">SEL2870</p>
                </div>
                <!-- end of example product -->
            </div>
        </div>
    </div>
</div>
~~~~