Reviews
======================== 

# Adding New Cameras/ Lenses Article

## Image

Thumbnail is using images from product `img/product/`  


Article images to be hosted by sony. Create an image folder on local for development purpose, send them the image folder, and they shall provide the images path.  


## Landing Page  


HTML is located at `reviews/index.template.html`.  

### Data

1. Data is located at `data/review_articles.json`.
2. Data for Camera is top half, and lenses is bottom half.
3. Way of filling the data
	* type : "camera" / "lens"
	* cate : "cameras" / "lenses"
	* name : name of the article
	* company : review resources
	* excerpt : short description of the article
	* url : to be same naming as the html files you create at `reviews/cameras/` or `reviews/lenses/`
	* img : Product image naming to be follow model number of end Product URL. (i.e. for a9 camera, its URL is `http://www.sony-asia.com/electronics/interchangeable-lens-cameras/ilce-9`, so the image naming will be `ilce-9`).

~~~~
{
    "type": "camera",
    "cate": "cameras",
    "name": "a7",
    "company": "By SLR Club",
    "excerpt": "The SONY a7 is an interchangeable lens mirrorless camera with a mounted 35mm full-frame image sensor (35.8 x 23.9mm)",
    "url": "a7",
    "img": "ilce-7-body-kit"
}
~~~~

~~~~
{
    "type": "lens",
    "cate": "lenses",
    "name": "FE 24-70mm F2.8 GM",
    "company": "By SLR Club",
    "excerpt": "Sony 24-70mm F2.8 GM lens is a premium standard zoom lens newly introduced by SONY that shows excellent resolution even to the peripheries with",
    "url": "sel2470gm",
    "img": "sel2470gm"
}
~~~~



## Article Content
1. Camera article is located at `reviews/cameras`, and lenses article is located at `reviews/lenses`.
2. `less/reviews.less` to edit CSS.
3. `js/reviews.js` to edit JS.


