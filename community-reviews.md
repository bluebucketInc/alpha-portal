Community Reviews 
======================== 

Community Reviews only accessible from certain product that has reviews under the `Explore more` section on ***[http://sony-asia-new.dev.interuptive.com/aportal/2015/product.html](http://sony-asia-new.dev.interuptive.com/aportal/2015/product.html)*** .

# Adding New Camera/ Lenses Article

## Image

1. Images is located at `img/community-reviews/camera` for Camera, and `img/community-reviews/lens` for Lenses.
2. Open a new folder with the naming of your product.


## Article Content
1. `community-reviews/` to add or delete article for cameras/lenses.
2. `less/community-reviews.less` to edit CSS.
3. `js/community-reviews.js` to edit JS.