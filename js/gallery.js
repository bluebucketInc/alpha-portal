$(function() {

    //==========================================
    // Select 2 :: Desktop
    //==========================================
    //
    //
    //
    //
    Handlebars.registerHelper('getPhotoSrc', function(data, options) {
        var out = data.substr(data.lastIndexOf("/")+1);
        return out;
    });

    $('#navbar > a.gallery').addClass('active');

    $("#filter-model, #filter-lenses, #filter-body, #filter-type").select2({
        minimumResultsForSearch: Infinity
    });

    $("#filter-categories").select2({
        minimumResultsForSearch: Infinity,
        templateResult: formatState
    });

    function formatState (state) {
      if (!state.id) { return state.text; }
      var $state = $(
        '<div class="cate-icon" style="background-image: url(img/gallery/filter/' + state.element.value.toLowerCase() + '.png);"></div>' + '<div class="cate-text">' + state.text + '</div>'
      );
      return $state;
    }

    // bind click and sync events to desktop
    $("#filter-model").on("select2:select", function (e) {
        gallery.setCameraModel($(this).val(), category);
        $("#filter-model-mobile").val($(this).val());
        gallery.renderGallery(gallery.getGallery(category));
    });

    $("#filter-lenses").on("select2:select", function (e) {
        gallery.setLensModel($(this).val(), category);
        $("#filter-lenses-mobile").val($(this).val());
        gallery.renderGallery(gallery.getGallery(category));
    });

    $("#filter-categories").on("select2:select", function (e) {
        gallery.setCategories($(this).val(), category);
        $("#filter-categories-mobile").val($(this).val());
        gallery.renderGallery(gallery.getGallery(category));
    });

    $("#filter-body").on("select2:select", function (e) {
        gallery.setBody($(this).val(), category);
        $("#filter-body-mobile").val($(this).val());
        gallery.renderGallery(gallery.getGallery(category));
    });

    $("#filter-type").on("select2:select", function (e) {
        gallery.setType($(this).val(), category);
        $("#filter-type-mobile").val($(this).val());
        gallery.renderGallery(gallery.getGallery(category));
    });


    $('div.filter-btn.clear').on("click", function(e) {
        gallery.clearFilters();
        gallery.renderGallery(gallery.getGallery(category));
        resetFilters();
    });

    // bind click and sync events to mobile
    $("#filter-model-mobile").on("change", function (e) {
        gallery.setCameraModel($(this).val(), category);
        $("#filter-model").val($(this).val()).trigger("change.select2");
        gallery.renderGallery(gallery.getGallery(category));
    });

    $("#filter-lenses-mobile").on("change", function (e) {
        gallery.setLensModel($(this).val(), category);
        $("#filter-lenses").val($(this).val()).trigger("change.select2");
        gallery.renderGallery(gallery.getGallery(category));
    });

    $("#filter-categories-mobile").on("change", function (e) {
        gallery.setCategories($(this).val(), category);
        $("#filter-categories").val($(this).val()).trigger("change.select2");
        gallery.renderGallery(gallery.getGallery(category));
    });

    $("#filter-body-mobile").on("change", function (e) {
        gallery.setBody($(this).val(), category);
        $("#filter-body").val($(this).val()).trigger("change.select2");
        gallery.renderGallery(gallery.getGallery(category));
    });

    $("#filter-type-mobile").on("change", function (e) {
        gallery.setType($(this).val(), category);
        $("#filter-type").val($(this).val()).trigger("change.select2");
        gallery.renderGallery(gallery.getGallery(category));
    });

    //! determine if it is cameras or lenses page
    var category = '';

    if (window.location.hash) {
        if (window.location.hash === '#photo') {
            category = 'photo';
            $('.filter-type').addClass('hidden');

        } else if (window.location.hash === '#video') {
            category = 'video';
            $('.filter-type').removeClass('hidden');

        } else {
            window.location.hash = 'photo';
            category = 'photo';
            $('.filter-type').addClass('hidden');
        }
    } else {
        window.location.hash = 'photo';
        category = 'photo';
        $('.filter-type').addClass('hidden');
    }

    $('.filter-category').find('.' + category).addClass('active');

    $('.filter-category').on("click", ".filter-btn:not(.active)", function(e) {
        $('.filter-btn.active').removeClass('active');
        category = $(this).data('category');
        window.location.hash = category;

        $('.filter-category').find('.' + category).addClass('active');

        gallery.clearFilters();
        resetFilters();

        trackCategory = category + 's';
        trackMs_link('aportal:gallery:'+trackCategory, 'aportal', 'aportal:gallery:'+trackCategory, 'microsite|aportal|gallery|'+trackCategory);

        if (category === 'video') {
            $('.filter-photos').addClass('hidden');
            $('.filter-videos').removeClass('hidden');
             gallery.renderVideos();
        } else if (category === 'photo') {
            $('.filter-photos').removeClass('hidden');
            $('.filter-videos').addClass('hidden');
            gallery.renderPhotos();
        }
    });

    if (category === 'photo') {
        $('.filter-videos').addClass('hidden');
        var preModel = getParameterByName('model');
        var preLense = getParameterByName('lens');
        if (preModel) {
            gallery.retrievePhotos(function() {
                gallery.setCameraModel(preModel, category);
                gallery.renderGallery(gallery.getGallery(category));
                $("#filter-model").val(preModel).trigger("change.select2");
                $("#filter-model-mobile").val(preModel);
            });
        } else {
            if (preLense) {
                gallery.retrievePhotos(function() {
                    gallery.setLensModel(preLense, category);
                    gallery.renderGallery(gallery.getGallery(category));
                    $("#filter-lenses").val(preLense).trigger("change.select2");
                    $("#filter-lenses-mobile").val(preLense);
                });
            } else {
                gallery.renderPhotos();
            }
        }
    } else if (category === 'video') {
        $('.filter-photos').addClass('hidden');
        gallery.renderVideos();
    }

    var resetFilters = function() {
        $("#filter-model").val(null).trigger("change");
        $("#filter-lenses").val(null).trigger("change");
        $("#filter-categories").val(null).trigger("change");
        $('#filter-body').val(null).trigger('change');
        $('#filter-type').val(null).trigger('change');
        $("#filter-model-mobile").val(null);
        $('#filter-lenses-mobile').val(null);
        $('#filter-categories-mobile').val(null);
        $('#filter-type-mobile').val(null);
        $('#filter-body-mobile').val(null);
    };

    $(window).on('resize', function() {
        gallery.justifyGallery(category);
    });
});

// gallery functions
(function(exports) {

    // gallery init options
    var cameraModel = "";
    var lensModel = "";
    var categories = "";
    var bodyModel = "";
    var typeName = "";
    var allPhotos = [];
    var filteredPhotos = [];
    var allVideos = [];
    var filteredVideos = [];

    var containerSelector = ".gallery-container";

    var magnificPopupOptions = {
        type: 'image',
        delegate: 'a',
        gallery: {
            enabled: true,
            preload: 0
        },
        image: {
            titleSrc: 'title',
            verticalFit: false,
        },
        closeBtnInside: false
    };
    var videoMagnificPopupOptions = {
        type: 'iframe',
        delegate: 'a',
        gallery: {
            enabled: true,
            preload: 0
        },
        closeBtnInside: false
    };
    var photoGallery = {
        templateSelector : "#photos-template",
        options : {
            rowheight : 200,
            waitThumbnailsLoad : false,
            margins : 5,
            captions : false,
            randomize: true
        },
        desktopHeight : 200,
        tabletHeight : 150,
        mobileHeight : 90,
        items : [],
        popUpOptions : magnificPopupOptions,
    };
    var videoGallery = {
        templateSelector : "#videos-template",
        options : {
            rowheight : 200,
            waitThumbnailsLoad : false,
            margins : 5,
            captions : true,
            randomize: true
        },
        desktopHeight : 200,
        tabletHeight : 150,
        mobileHeight : 90,
        items : [],
        popUpOptions : videoMagnificPopupOptions
    };
    // gallery setters
    exports.setCameraModel = function(e, galleryCategory) {
        cameraModel = e;
        exports.filterPhotos();
    };
    exports.setLensModel = function(e, galleryCategory) {
        lensModel = e;
        exports.filterPhotos();
    };
    exports.setCategories = function(e, galleryCategory) {
        categories = e;
        exports.filterPhotos();
    };
    exports.setBody = function(e, galleryCategory) {
        bodyModel = e;
        exports.filterVideos();
    };
    exports.setType = function(e, galleryCategory) {
        typeName = e;
        exports.filterVideos();
    };
    exports.clearFilters = function(galleryCategory) {
        photoGallery.items = allPhotos;
        videoGallery.items = allVideos;
        cameraModel = "";
        lensModel = "";
        categories = "";
        bodyModel = "";
        typeName = "";
    };
    exports.retrievePhotos = function(callback) {
        if (_.isEmpty(allPhotos)) {
            $.ajax({
                url: 'data/photos.json',
                type: 'GET',
                dataType: 'json'
            })
            .done(function(data) {
                allPhotos = data;
                photoGallery.items = data;
                callback();
            })
            .always(function() {

            });
        } else {
            callback();
        }
    };
    exports.retrieveVideos = function(callback) {
        if (_.isEmpty(allVideos)) {
            $.ajax({
                url: 'data/videos.json',
                type: 'GET',
                dataType: 'json'
            })
            .done(function(data) {
                allVideos = data;
                videoGallery.items = data;
                callback();
            })
            .always(function() {

            });
        } else {
            callback();
        }
    };
    exports.renderPhotos = function() {
        exports.retrievePhotos(function() {
            exports.renderGallery(photoGallery);
        });
    };
    exports.renderVideos = function() {
        exports.retrieveVideos(function() {
            exports.renderGallery(videoGallery);
        });
    };
    exports.filterPhotos = function() {
        filteredPhotos = _.filter(allPhotos, function(n) {
                return (n.camera === cameraModel || _.isEmpty(cameraModel)) &&
                       (n.lens === lensModel || _.isEmpty(lensModel)) &&
                       (_.includes(n.categories,categories) || _.isEmpty(categories));
        });

        photoGallery.items = filteredPhotos;
    };
    exports.filterVideos = function() {
        filteredVideos = _.filter(allVideos, function(n) {
                return (n.bodyModel === bodyModel || _.isEmpty(bodyModel)) &&
                        (n.typeName === typeName || _.isEmpty(typeName));
        });
        videoGallery.items = filteredVideos;
    };
    exports.renderGallery = function(gallery) {

        var template = Handlebars.compile($(String(gallery.templateSelector)).html());
        var html = stripStupidSpacesFrontAndBack(template(gallery.items));

        $(String(containerSelector)).html(html);

        exports.justifyGallery(gallery);

    };
    exports.justifyGallery = function(gallery) {

        if (gallery.items <= 0) {
            return $(containerSelector).html('<p class="coming_soon">No results found.</p>');
        }

        var init = false;

        if (gallery.options === undefined) {
            gallery = exports.getGallery(gallery);
        }

        if ($(window).width() <= 640) {
            gallery.options.rowHeight = gallery.mobileHeight;
        } else if ($(window).width() < 1250) {
            gallery.options.rowHeight = gallery.tabletHeight;
        } else {
            gallery.options.rowHeight = gallery.desktopHeight;
        }

        $(containerSelector).justifiedGallery(gallery.options).on('jg.complete', function () {
            if (!init) {
                $(this).magnificPopup(gallery.popUpOptions);
                init = true;
            }
        }).on('jg.resize', function () {
            if (!init) {
                $(this).magnificPopup(gallery.popUpOptions);
                init = true;
            }
        });
    };
    exports.getGallery = function(galleryCategory) {
        switch (galleryCategory) {
            case "photo" :
            return photoGallery;
            case "video" :
            return videoGallery;
            default:
            return photoGallery;
        }
    };
})(this.gallery = {});

function stripStupidSpacesFrontAndBack(str) {
    return str.replace(/^[\s\t\n\r\u200B]+|[\s\t\n\r\u200B]+$/g, '');
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
