$(function() {

	 $('#navbar > a.ambassadors').addClass('active');
	
	//---------------------------------------
	// LAZYYT FOR BROWSERS EXCEPT FOR CHROME
	//---------------------------------------
	/*var isChrome = !!window.chrome;

	$(window).load(function() {
		// console.log(isChrome);
		if (!isChrome || navigator.userAgent.match(/Android/i)) {
            $('.lazyyt').each(function() {
                $(this).replaceWith('<iframe width="1024" height="576" src="https://www.youtube.com/embed/' + $(this).data('youtubeId') + '" frameborder="0" allowfullscreen></iframe>');
            });
        }
	});
		

	$('.lazyyt').lazyYT().removeClass('lazyYT-video-loaded');*/



	//---------------------------------------
	// LAZYLOAD FOR IMAGES
	//---------------------------------------
	$('img.lazy').lazyload({
            effect: 'fadeIn'
        });


	$(".ambassador-photo-thumbs").slick({
		infinite: true,
		speed: 300,
		slidesToShow: 6,
		responsive: [
            {
             breakpoint: 1000,
             settings: {
                    slidesToShow: 4
                }
            },
            {
             breakpoint: 768,
             settings: {
                    slidesToShow: 3
                }
            }
        ]
	})
	$(".ambassador-photo-thumbs img").click(function(){
		var a = $(this).data("id");
		var am = $(".ambassador-photo-thumbs").data('ambassador');
		$("img.imgborder").removeClass('imgborder');
		$(this).addClass('imgborder');
		$(".hero-photo").hide();
		$(".hero-photo#"+am+"_" + a).show();
		$(".ambassadar-photo-description p").hide();
		$(".ambassadar-photo-description #description_" + a).show();
		$(".mobile-ambassadar-photo-description p").hide();
		$(".mobile-ambassadar-photo-description #mobile_description_" + a).show();

	});
	$(".ambassador-accordion-title").click(function(){
		if($(this).hasClass('expanded')){
			$(this).removeClass('expanded');
			$(this).next('.ambassador-accordion-content').slideUp();
		} else {
			$(this).addClass('expanded');
			$(this).next('.ambassador-accordion-content').slideDown();
		}
	});
	$("#mobile-description-toggle").click(function(){
		if($(this).hasClass('shown-details')){
			$(".mobile-ambassadar-photo-description").slideUp();
			$(this).removeClass('shown-details');
			$(this).text('+ Show Details');
		} else {
			$(".mobile-ambassadar-photo-description").slideDown();
			$(this).addClass('shown-details');
			$(this).text('- Hide Details');
		}
	})

	adjustVidDimension();

	$(window).resize(function(){
		adjustVidDimension();
	})

	function adjustVidDimension(){
		// var x = $("#ambassador-vid").outerWidth();
		// console.log(x);
		// $("#ambassador-vid").height( x/560*315);
		if(window.innerWidth <= 630){
			$("#ambassador-vid").width(window.innerWidth - 70);
			var aw = $("#ambassador-vid").width();
			$("#ambassador-vid").height( aw/560*315);
		}	
	}
});

/*
function setIframe() {
    if (window.location.host == "sony-asia.com") {
        var cssLink = document.createElement("link");
        // to change css
        cssLink.href = "http://www.sony-asia.com/microsite/ilc/css/iframeoverwrite.css";
        cssLink.rel = "stylesheet";
        cssLink.type = "text/css";
        frames[0].onload = function() {
            frames[0].document.head.appendChild(cssLink);
        };
    }
}
*/

