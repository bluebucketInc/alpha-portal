$(function() {

    Handlebars.registerHelper('getPhotoSrc', function(data, options) {
        var out = data.substr(data.lastIndexOf("/")+1);
        return out;
    });

    $('#navbar > a.swpa').addClass('active');

    $("#filter-competition, #filter-country").select2({
        minimumResultsForSearch: Infinity
    });

    $("#filter-categories").select2({
        minimumResultsForSearch: Infinity
    });

    $('.swpa-categories-container').slick({
      arrows: true,
      nextArrow: '<div class="arrow-container arrow-right"><button type="button" class="slick-next">Next</button></div>',
      prevArrow: '<div class="arrow-container arrow-left"><button type="button" class="slick-prev">Prev</button></div>',
      dots: false,
      accessibility: true,
      draggable: false,
      slidesToShow: 5,
      slidesToScroll: 5,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
                slidesToShow: 5,
                slidesToScroll: 5
            }
        },
        {
          breakpoint: 1199,
          settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
          breakpoint: 1024,
          settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }
      ]
    });

    var state = '';

    if ($('div.filter-swpa-categories').is(':visible')) {
        state = 'desktop';
    } else {
        state = 'mobile';
    }
    swpaWinners.renderPhotos();

    // bind click and sync events to desktop
    $("#filter-competition").on("select2:select", function (e) {
        var competition = $(this).val();
        swpaWinners.setCompetition(competition);
        $("#filter-competition-mobile").val(competition);
        swpaWinners.renderGallery();
        hideCountry(competition);
    });

    $("#filter-country").on("select2:select", function (e) {
        swpaWinners.setCountry($(this).val());
        $("#filter-country-mobile").val($(this).val());
        swpaWinners.renderGallery();
    });

    $('.filter-swpa-categories').on("click", ".filter-btn:not(.active)", function (e) {
        $(this).addClass('active');
        var categories = [];
        $('.filter-btn.active').each(function(index){
            categories.push($(this).data('categories'));
        });
        $("#filter-swpa-categories-mobile").val(categories[0]);
        swpaWinners.setCategories(categories);
        swpaWinners.renderGallery();
    });

    $('.filter-swpa-categories').on("click", ".filter-btn.active", function (e) {
        $(this).removeClass('active');
        var categories = [];
        $('.filter-btn.active').each(function(index){
            categories.push($(this).data('categories'));
        });
        categories.length > 0 ? $("#filter-swpa-categories-mobile").val(null) : $("#filter-swpa-categories-mobile").val(categories[0]);
        swpaWinners.setCategories(categories);
        swpaWinners.renderGallery();
    });

    // bind click and sync events to mobile
    $("#filter-competition-mobile").on("change", function (e) {
        var competition = $(this).val();
        swpaWinners.setCompetition(competition);
        $("#filter-competition").val(competition).trigger("change.select2");
        swpaWinners.renderGallery();
        hideCountry(competition);
    });

    $("#filter-country-mobile").on("change", function (e) {
        swpaWinners.setCountry($(this).val());
        $("#filter-country").val($(this).val()).trigger("change.select2");
        swpaWinners.renderGallery();
    });

    $("#filter-swpa-categories-mobile").on("change", function (e) {
        var categories = $(this).val() === '' ? [] : [$(this).val()];
        swpaWinners.setCategories(categories);
        swpaWinners.renderGallery();
    });

    $('div.filter-btn.clear').on("click", function(e) {
        swpaWinners.clearFilters();
        swpaWinners.renderGallery();
        resetFilters();
    });

    var resetFilters = function() {
        $("#filter-competition").val(null).trigger("change");
        $("#filter-country").val(null).trigger("change");
        $(".filter-btn.active").removeClass('active');

        $("#filter-competition-mobile").val(null);
        $('#filter-country-mobile').val(null);
        $('#filter-swpa-categories-mobile').val(null);

        unhideCountry();
    };

    // on window resize, justify gallery
    $(window).on('resize', function() {
        var newState = '';
        if ($('div.filter-swpa-categories').is(':visible')) {
            newState = 'desktop';
        } else {
            newState = 'mobile';
        }
        if (state !== newState) {
            state = newState;
            swpaWinners.clearFilters();
            resetFilters();
            swpaWinners.renderGallery();
        } else {
            swpaWinners.justifyGallery();
        }
    });

    // common functions
    var hideCountry = function(competition) {
        if (competition == 'youth' || competition == 'open' || competition == 'professional') {
            $('div.filter-country').css('visibility', 'hidden');
        } else {
            $('div.filter-country').css('visibility', 'visible');
        }
    }
    var unhideCountry = function() {
        $('div.filter-country').css('visibility', 'visible');
    }
});

// swpa winners functions
(function(exports) {

    // swpa winners init options
    var competition = "";
    var country = "";
    var categories = [];
    var allPhotos = [];

    var containerSelector = ".gallery-container";

    var magnificPopupOptions = {
        type: 'inline',
        delegate: 'a',
        gallery: {
            enabled: true,
            preload: 0
        },
        closeBtnInside: false,
        closeOnBgClick: true,
        callbacks: {
            open: function() {
                // console.log(this);
            }
        }
    };
    var photoGallery = {
        templateSelector : "#photos-template",
        options : {
            rowheight : 200,
            waitThumbnailsLoad : false,
            margins : 5,
            captions : false,
            randomize: true,
            selector: 'a'
        },
        desktopHeight : 200,
        tabletHeight : 150,
        mobileHeight : 90,
        items : [],
        popUpOptions : magnificPopupOptions,
    };

    // swpa winners setters
    exports.setCompetition = function(e) {
        competition = e;
        exports.filterPhotos();
    };
    exports.setCountry = function(e) {
        country = e;
        exports.filterPhotos();
    };
    exports.setCategories = function(e) {
        categories = e;
        exports.filterPhotos();
    };
    exports.clearFilters = function(galleryCategory) {
        photoGallery.items = allPhotos;
        competition = "";
        country = "";
        categories = [];
    };
    exports.retrievePhotos = function(callback) {
        if (_.isEmpty(allPhotos)) {
            $.ajax({
                url: 'data/swpa.json',
                type: 'GET',
                dataType: 'json'
            })
            .done(function(data) {
                allPhotos = data;
                photoGallery.items = data;
                callback();
            })
            .always(function() {

            });
        } else {
            callback();
        }
    };
    exports.renderPhotos = function() {
        exports.retrievePhotos(function() {
            exports.renderGallery(photoGallery);
        });
    };
    exports.filterPhotos = function() {
        filteredPhotos = _.filter(allPhotos, function(n) {
                return (n.competition === competition || _.isEmpty(competition)) &&
                       (n.country === country || _.isEmpty(country)) &&
                       ((_.every(categories, function (category) {
                           return _.includes(n.categories, category);
                       })) || _.isEmpty(categories))
        });
        photoGallery.items = filteredPhotos;
    };
    exports.renderGallery = function() {
        var template = Handlebars.compile($(String(photoGallery.templateSelector)).html());
        var html = stripStupidSpacesFrontAndBack(template(photoGallery.items));

        $(String(containerSelector)).html(html);

        exports.justifyGallery(photoGallery);
    };
    exports.justifyGallery = function() {

        if (photoGallery.items <= 0) {
            return $(containerSelector).html('<p class="coming_soon">No results found.</p>');
        }

        var init = false;

        if ($(window).width() <= 640) {
            photoGallery.options.rowHeight = photoGallery.mobileHeight;
        } else if ($(window).width() < 1250) {
            photoGallery.options.rowHeight = photoGallery.tabletHeight;
        } else {
            photoGallery.options.rowHeight = photoGallery.desktopHeight;
        }

        $(containerSelector).justifiedGallery(photoGallery.options).on('jg.complete', function () {
            if (!init) {
                $(this).magnificPopup(photoGallery.popUpOptions);
                init = true;
            }
        }).on('jg.resize', function () {
            if (!init) {
                $(this).magnificPopup(photoGallery.popUpOptions);
                init = true;
            }
        });
    };
})(this.swpaWinners = {});

function stripStupidSpacesFrontAndBack(str) {
    return str.replace(/^[\s\t\n\r\u200B]+|[\s\t\n\r\u200B]+$/g, '');
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
