$(function() {

	//  $('#navbar > a.reviews').addClass('active');
	// console.log('jj');
	//-----------------------------------
    // Sticky Menu bar
    //-----------------------------------

    // Window width
    // var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
    // var topofnav = $('.comm-secondary-nav').offset().top + $('.intro').parent().height() + $('.navbar-fixed-top').height() + 280;
    // if (windowWidth > 1023) {
    //     $(window).on('scroll', function() {
    //         $('.comm-secondary-nav').toggleClass('fixed', $(window).scrollTop() > topofnav);
    //     });
    //       $('.comm-secondary-nav a[href*=#]:not([href=#])').click(function() 
		  // {
		  //   if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
		  //       || location.hostname == this.hostname) 
		  //   {
		      
		  //     var target = $(this.hash),
		  //     headerHeight = $(".primary-header").height() + 5; // Get fixed header height
		            
		  //     target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		              
		  //     if (target.length) 
		  //     {
		  //       // $('html,body').animate({
		  //       //   scrollTop: target.offset().top - headerHeight
		  //       // }, 500);
		  //       $.smoothScrollTo(target, { duration: 600, step: false })
		  //       return false;
		  //     }
		  //   }
		  // });
    // }

    //--------------------------------------------------
  //  DOM Ready
  //--------------------------------------------------
  $(document).ready(function (){
    // $('.mvisual').fitText({
    //   breakPoint: [1200, 640, 480],
    //   dataMin: 320, delay: 200, self: true
    // });
    if($('.comm-secondary-nav, .secondary-nav-mobile').is(':visible')){
    	
	    $('nav.menu').setMenu();
	    $('.movie-box').movieBox('ja');
      $('#content').addClass('paddingwithsecnav');
    }
  });

    //--------------------------------------------------
  //  Set Menu
  //--------------------------------------------------
  $.fn.setMenu = function() {
    var $menu = $(this);
    var option = { duration: 600, step: false }
    // var $clone = $menu.clone();
    
    // $clone.addClass('menu-clone');
    // $menu.after($clone);
    $menu.cloneMenu();
    
    $('li a', $menu).each(function() {
      var $ot = $('img', this);
      // var src = $ot.attr('src').replace('.png', 'on.png');
      var $on = $('<img />').attr('src');
      var $otbox = $('<div class="ot" />').append($ot);
      var $onbox = $('<div class="on" />').append($on);
      
      $(this).append($otbox, $onbox);
    });
    
    $('li a', $menu).on('click', function(e) {
      var href = $(this).attr('href');
      $.smoothScrollTo($(href), option);
      // console.log($(href));
      return false;
    });
    
    $menu.floatMenu();
    
    return this;
  }
  
  // --------------------------------------------------
  //  Float Menu
  // --------------------------------------------------
  $.fn.floatMenu = function() {
    var FLOAT_CLASS = 'menu--floating';
    var $window = $(window);
    var $container = $(this);
    var $floater = $('.menu-wrap', $container);
    var $menu = $('li', $floater);
    var $cloneWrap = $('.menu-wrap');
    var $section = $('#concept, #auto-focus, #operability, #image-quality, #4K-movie, #design');
    
    var isFloating = false;
    var cloneVisible = false;
    var cloneFloationg = false;
    var offsetTop, fheight;
    // var cloneTop, cloneH;
    var scrollTimer, resizeTimer;
    
    var update = function() {
      offsetTop = $container.offset().top;
      fheight   = $floater.outerHeight();
      // cloneTop = $clone.offset().top;
      // cloneH = $cloneWrap.outerHeight();
      
      $container.css({ height: fheight });
      cloneVisible = $floater.css('position') === 'static';
      
      // var h = (!cloneVisible)? fheight : cloneH;
      $section.css({ 'margin-top': -fheight, 'padding-top': fheight });
    }
    
    var detect = function(scrollTop) {
      var index;
      
      $section.each(function(i) {
        var top = $(this).offset().top;
        // if (i != 0)
        top = top - 1;
        if (scrollTop >= top) index = i;
        // if (scrollTop < top) index = 0;
        // console.log($(this));
        // console.log(scrollTop);
      });
      
      if (cloneVisible && (offsetTop - scrollTop) < 0) {
        if (!cloneFloationg) {
          cloneFloationg = true;
          $cloneWrap.addClass(FLOAT_CLASS);
        }
      }
      
      // console.log(index);
      $menu.removeClass('current').eq(index).addClass('current');
    }
    
    $window.on('scroll.floatMenu', function(e) {
      var scrollTop = $window.scrollTop();
      offsetTop = $container.offset().top;
      
      if ((offsetTop - scrollTop) < 0) {
        if (!isFloating) {;
          isFloating = true;
          $container.addClass(FLOAT_CLASS);
        }
      }
      else if ((offsetTop - scrollTop) >= 0) {
        if (isFloating) {;
          isFloating = false;
          $container.removeClass(FLOAT_CLASS);
        }
      }
      
      if (cloneVisible && (offsetTop - scrollTop) >= 0) {
        if (cloneFloationg) {
          cloneFloationg = false;
          $cloneWrap.removeClass(FLOAT_CLASS);
        }
      }
      
      if (scrollTimer !== false) clearTimeout(scrollTimer);
      scrollTimer = setTimeout(function() {
        detect(scrollTop);
      }, 50);
    })
    .on('resize.floatMenu', function(e) {
      if (resizeTimer !== false) clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {
        update();
      }, 50);
    })
    .on('load.floatMenu', function(e) {
      update();
      $(this).off(e).trigger('scroll');
    });
    
    update();
    
    return this;
  }
  
  //--------------------------------------------------
  //  Clone Menu
  //--------------------------------------------------
  $.fn.cloneMenu = function() {
   //  var $container = $(this);
    // var $section = $('#concept, #auto-focus, #operability, #image-quality, #4K-movie, #design');
  	// var update = function() {
   //    offsetTop = $container.offset().top;
   // var fheight   = $('.menu-wrap', $(this)).outerHeight();
   //    // cloneTop = $clone.offset().top;
   //    // cloneH = $cloneWrap.outerHeight();
      
   //    $container.css({ height: fheight });
   //    cloneVisible = $floater.css('position') === 'static';
      
      // var h = (!cloneVisible)? fheight : cloneH;
      // $section.css({ 'margin-top': -($(this).outerHeight()), 'padding-top': ($(this).outerHeight()) });
    // }
  	$('#comm-reviews-mobile-nav').on('change', function(){
  		// console.log($('.secondary-nav-mobile').outerHeight() + $('header').outerHeight());
  		$.smoothScrollTo($('#' + $(this).val()), { duration: 600, offsetMenu: true });
  	});
    // var SLIDE_DURATION = 200;
    // var SLIDE_EASING = 'easeInOutQuad';
    // var ADD_CLASS = 'menu--open';
    // var DOC_EVENT = 'click.cloneMenu touchend.cloneMenu';
    
    // var template = [
    //   '<div class="toggle-inner">',
    //     '<div class="toggle">',
    //       '<span class="bar1"></span>',
    //       '<span class="bar2"></span>',
    //       '<span class="bar3"></span>',
    //     '</div>',
    //   '</div>'
    // ].join('');
    
    // var $menu = $(this);
    // var $inner = $('.menu-inner', $menu);
    // var $anchor = $('a', $inner);
    // var $template = $(template);
    // var $toggle = $('.toggle', $template);
    // var $backdrop = $('<div class="backdrop" />');
    // var isOpen = false;
    
    // $toggle.on('click.cloneMenu', function(e) {
    //   if (!isOpen) {
    //     $menu.addClass(ADD_CLASS);
    //     $inner.slideDown(SLIDE_DURATION, SLIDE_EASING, function() {
    //       isOpen = true;
    //     });
    //   }
    //   else {
    //     $menu.removeClass(ADD_CLASS);
    //     $inner.slideUp(SLIDE_DURATION, SLIDE_EASING, function() {
    //       isOpen = false;
    //     });
    //   }
      
    // });
    
    // $backdrop.on(DOC_EVENT, function(e) {
    //   if (isOpen) $toggle.click();
    // });
    
    // $anchor.on('click.cloneMenu', function(e) {
    //   var href = $(this).attr('href');
    //   if (isOpen) $toggle.click();
    //   setTimeout(function() {
        

    //   // console.log($.smoothScrollTo($(href), { duration: 600 }));
    //   }, SLIDE_DURATION + 100);
    //   return false;
    // });
	
    // $('.menu-wrap', $menu).prepend($template);
    // $menu.append($backdrop);
  }
  
  //--------------------------------------------------
  //  Movie Box
  //--------------------------------------------------
  $.fn.movieBox = function(language) {
    var $movieBox = $(this);
    var tag = document.createElement('script');
    var firstScriptTag = document.getElementsByTagName('script')[0];
    tag.src = "https://www.youtube.com/iframe_api";
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    
    window.onYouTubeIframeAPIReady = function() {
      $movieBox.each(function(i) {
        var $this = $(this);
        var videoID = $this.attr('data-videoID');
        var start = $this.attr('data-start') || 0;
        
        if (videoID === undefined || videoID === '') return;
        
        var $container = $('<div class="video-container">');
        var $video = $('<div class="video-inner" />');
        var player;
        
        var onPlayerStateChange = function(e) {
          if (e.data === 1 || e.data === 3) {
            $this.addClass('video--isPlaying');
          }
        }
        
        $video.attr('id', videoID);
        $container.append($video);
        $this.append($container);
        
        player = new YT.Player(videoID, {
          width: '640',
          height: '360',
          videoId: videoID,
          playerVars: {
            'hl': language,
            'start': start,
            'autohide': 1,
            'showinfo': 0,
            'controls': 1,
            'rel': 0,
            'cc_load_policy': 0,
            'wmode': 'transparent',
            'modestbranding': 1
          },
          events: {
          'onStateChange': onPlayerStateChange
          }
        });
      });
    }
    
    return this;
  }
  
  //--------------------------------------------------
  //  Scroll To
  //--------------------------------------------------
  $.smoothScrollTo = function($target, options) {
    var webkit = 'WebkitAppearance' in document.documentElement.style;
    var tag = (webkit)? 'body' : 'html';
    
    var opt = $.extend({
      duration: 300,
      easing  : 'swing',
      step    : false,
      buffer  : 0,
      offsetMenu: false
    }, options);
    
    var $window = $(window);
    var speed = opt.duration;
    var offsetHeight = document.body.offsetHeight;

    // var clientHeight = document.documentElement.clientHeight || window.innerHeight;
    var clientHeight = document.documentElement.clientHeight || document.body.clientHeight;
    var position = Math.floor($target.offset().top);
    var dif = offsetHeight - position;
    
    if (opt.offsetMenu) {
    	// console.log(offsetHeight);
    	position = position - ($('.secondary-nav-mobile').outerHeight() + $('header').outerHeight());
    }
    // if (position !== 0) position -= opt.buffer;
    // if (dif < clientHeight) position -= clientHeight - dif;
    
    if (opt.step) {
      var distance = Math.abs($window.scrollTop() - position);
      var friction = Math.ceil((distance/($window.height()*2.3))*10)/10;
      speed = opt.duration * friction;
      if (speed < opt.duration) speed = opt.duration;
     }
    
    $(tag).delay(50).stop()
    .animate({ scrollTop: position }, speed, opt.easing)
    .queue(function() {
      $(this).dequeue();
    });
    // console.log($target);
    // console.log(position);
    // console.log(clientHeight);
  }

	//---------------------------------------
	// LAZYYT FOR BROWSERS EXCEPT FOR CHROME
	//---------------------------------------
	var isChrome = !!window.chrome;

	$(window).load(function() {
		// console.log(isChrome);
		if (!isChrome || navigator.userAgent.match(/Android/i)) {
            $('.lazyyt').each(function() {
                $(this).replaceWith('<iframe width="1024" height="576" src="https://www.youtube.com/embed/' + $(this).data('youtubeId') + '" frameborder="0" allowfullscreen></iframe>');
            });
        }
	});
		

	$('.lazyyt').lazyYT().removeClass('lazyYT-video-loaded');



	//---------------------------------------
	// LAZYLOAD FOR IMAGES
	//---------------------------------------
	$('img.lazy').lazyload({
            effect: 'fadeIn'
        });


	

	if (/iPad|iPhone|iPod/.test(navigator.platform)) {
	    $("div.model-item").on('click', function() {
	        window.open($(this).attr('href'));
	    });
	} else {

		$("div.box-cta").attr('href', function(i, val) {
		    return val + "#page-main-content";
		});
		
	    $("div.model-item").colorbox({
	        iframe: true,
	        width: "95%",
	        height: "95%",
	        onComplete: setIframe
	    });
	}
});

function setIframe() {
    if (window.location.host == "sony-asia.com") {
        var cssLink = document.createElement("link");
        // to change css
        cssLink.href = "http://www.sony-asia.com/microsite/ilc/css/iframeoverwrite.css";
        cssLink.rel = "stylesheet";
        cssLink.type = "text/css";
        frames[0].onload = function() {
            frames[0].document.head.appendChild(cssLink);
        };
    }
}
