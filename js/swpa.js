$(function() {
    $('#navbar > a.swpa').addClass('active');
    swpa.renderPhoto();
});

// swpa functions
(function(exports) {

    // swpa init options
    var allPhotos = [];
    var templateSelector = "#swpa-photo-template";
    var containerSelector = '#photo-of-the-day';

    // swpa setters
    exports.retrievePhotos = function(callback) {
        if (_.isEmpty(allPhotos)) {
            $.ajax({
                url: 'data/swpa.json',
                type: 'GET',
                dataType: 'json'
            })
            .done(function(data) {
                allPhotos = data;
                callback();
            })
            .always(function() {

            });
        } else {
            callback();
        }
    };
    exports.renderPhoto = function() {
        exports.retrievePhotos(function() {
            var photo = _.sample(allPhotos);
            var template = Handlebars.compile($(String(templateSelector)).html());
            var html = stripStupidSpacesFrontAndBack(template(photo));

            $(String(containerSelector)).html(html);
        });
    };
})(this.swpa = {});

function stripStupidSpacesFrontAndBack(str) {
    return str.replace(/^[\s\t\n\r\u200B]+|[\s\t\n\r\u200B]+$/g, '');
}
