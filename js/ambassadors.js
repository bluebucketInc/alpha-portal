'use strict'

$(function() {

    $('#navbar > a.ambassadors').addClass('active');

    Handlebars.registerHelper('equals', function(val1, val2, options) {

        if (arguments.length < 3)
            throw new Error("Handlebars Helper equal needs 2 parameters");

        if (val1 !== val2) {
            return options.inverse(this);

        } else {
            return options.fn(this);
        }
    });

    Handlebars.registerHelper('reviewsRenameTracking', function(text) {
        // text = Handlebars.Utils.escapeExpression(text);
        // console.log(text);
        // console.log(text.toLowerCase().replace('&trade;', '').replace(/[^\w\d\s]+/g, '').replace(/\ +/g, ''));
        return text.toLowerCase().replace('&trade;', '').replace(/[^\w\d\s]+/g, '').replace(/\ +/g, '');
    });

    $.ajax({
            url: 'data/ambassadors.json',
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            console.log("success");

            var target = $("#ambassadors-container")[0];

            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    var newNodes = mutation.addedNodes; // DOM NodeList

                    if (newNodes !== null) { // If there are new nodes added
                        var $nodes = $(newNodes); // jQuery set

                        $nodes.each(function() {
                            var $node = $(this);

                            if ($node.hasClass('box-wrapper')) {

                                $node.find('.thumb').load(function() {
                                    $('.ambassadors-container .box-wrapper').standardHeight();
                                    // $('.box-wrapper').height(tabulateTallestHeight('.box-wrapper') + $('.cta').height() + 25);
                                });
                            }
                        });
                    }
                });
            });

            var config = {
                attributes: true,
                childList: true,
                subtree: true
            };

            observer.observe(target, config);

            try {
                var itemsTemplate = Handlebars.compile($('#items-template').html());
                var itemsHTML = stripStupidSpacesFrontAndBack(itemsTemplate(data));
                $('#items-template').replaceWith(itemsHTML);


            } catch (e) {
                console.log(e);
            }


            // Set equal height for multiple elements
            $(window).load(function() {
                $('.reviews-container .box-wrapper').standardHeight();
            });



            $(window).resize();
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });


    //! like the function name has said
    function stripStupidSpacesFrontAndBack(str) {
        return str.replace(/^[\s\t\n\r\u200B]+|[\s\t\n\r\u200B]+$/g, '');
    }





});
