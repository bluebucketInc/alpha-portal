$(function() {


	//-----------------------------------
	// Copyright Year
	//-----------------------------------
	copyrightYear();
	function copyrightYear() {
	   $("#year").html(new Date().getFullYear());
	}

	//-----------------------------------
	// Navbar maxheight for small screen  
	//-----------------------------------

	var windowHeight = window.innerHeight;

	if((windowHeight >= 319) && (windowHeight < 400)){
		$('.navbar-fixed-bottom .navbar-collapse, .navbar-fixed-top .navbar-collapse').addClass("navbar-maxheight")
	}


	//-----------------------------------
	// slick
	//-----------------------------------

	$('.home-slider-thumb').slick({
		// autoplay: true,
		arrows: false,
		slidesToShow: 4,
		// slidesToScroll: 1,
		// dots: true,
		// centerMode: true,
		draggable: false,
		asNavFor: '.home-slider',
		focusOnSelect: true
	});
	$('.home-slider').slick({
		pauseOnFocus: true,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: false,
		dots: true,
		accessibility: true,
		draggable: false,
		swipe: true,
		asNavFor: '.home-slider_thumb',
		responsive: [
			{
				breakpoint: 768,
				setting: {
					dots: true
				}
			}
		]
	});


	$('.home-slider').on('afterChange', function( slick, currentSlide, nextSlide){
		var num = nextSlide;

		$(".slick-slide").siblings().removeClass("slick-current");
		$(".home-thumb-" + (num+1)).parent().addClass("slick-current");
	});





	if (/iPad|iPhone|iPod/.test(navigator.platform)) {
	    $("div.btn-cta").on('click', function() {
	        window.open($(this).attr('href'));
	    });
	} else {

		// $("div.box-cta").attr('href', function(i, val) {
		//     return val + "#page-main-content";
		// });

	    $("div.btn-cta").colorbox({
	        iframe: true,
	        width: "95%",
	        height: "95%",
	        onComplete: setIframe
	    });
	}


	function setIframe() {
        if (window.location.host == "sony-asia.com") {
            var cssLink = document.createElement("link");
            // to change css
            cssLink.href = "http://www.sony-asia.com/microsite/ilc/css/iframeoverwrite.css";
            cssLink.rel = "stylesheet";
            cssLink.type = "text/css";
            frames[0].onload = function() {
                frames[0].document.head.appendChild(cssLink);
            };
        }
    }


});
