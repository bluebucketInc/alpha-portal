(function ($) {
  "use strict";

  $('#navbar > a.shooting-techniques').addClass('active');

  function makeAlias(object, name) {
      var fn = object ? object[name] : null;
      if (typeof fn == 'undefined') return function () {};
      return function () {
          return fn.apply(object, arguments);
      };
  }
  var $viewer = $('#shooting-viewer');
  var $frame = $viewer.find('.frame');
  var $mapFrame = $viewer.find('.mapFrame .positionWrapper');
  var $mapElements = $viewer.find('.mapFrame .positionWrapper .mapElements');
  var $imageFrame = $viewer.find('.imageFrame');
  var $control = $('#shooting-control');
  var $stepper = $control.find('.stepper');
  var $title = $control.find('.title');
  var $meta = $control.find('.meta');
  var __el = makeAlias(document, 'createElement');
  var lensRecommendations;
  var lensPopulated = false;
  var BASE_URL = 'img/shoot/';

  var layoutMapOnFrame = function(mapInfo, $frame, basePath) {
    var $img = $frame.find('img');
    var imageHeight = $img.height();
    var imageWidth = $img.width();
    var baseWidth = mapInfo.baseWidth;
    var baseHeight = mapInfo.baseHeight;

    var translateX = function(x) {
      return (x / baseWidth * 100) + '%';
    };
    var translateY = function(y) {
      return (y / baseHeight * 100) + '%';
    };
    var pinClicked = function ($pin) {
      var set = $pin.attr('data-set');
      var effect = $.data($pin.get(0), 'effect');
      if (! $pin.hasClass('active')) {
        $pin.siblings('.pin').popover('hide');
        $pin.siblings('.pin').hide();
        $('.pin[data-set='+set+']').show().addClass('active').html('');
        $pin.popover('show');
        $('.popover').css({
          left: translateX(effect.description.position.x),
          top: translateY(effect.description.position.y),
        });
        $pin.addClass('active');
        if (effect.effect) {
          var $effectOverlay = $(__el('img')).attr({
            'src': basePath + effect.effect,
            'class': 'effect-overlay'})
          .css({
            'left': translateX(0),
            'top': translateY(0),
            'width': '100%',
            'height': 'auto'
          });
          $frame.append($effectOverlay);
        }
      } else {
        $('.pin').show();
        $('.pin[data-set='+set+']').html(set).popover('hide').removeClass('active');
        $frame.find('.effect-overlay').remove();
      }
    };

    // populate pins
    _(mapInfo.pins).each(function(pin, index) {
      var $pin = $(__el('div')).addClass('pin');
      var effect = mapInfo.effects[pin.set - 1];

      $pin.append(pin.set);
      $pin.attr('data-set', pin.set);
      $pin.css({
        left: translateX(pin.x),
        top: translateY(pin.y)
      });
      $pin.popover({
        content: effect.description.content,
        trigger: 'manual',
        container: '#shooting-viewer .mapFrame .positionWrapper .mapElements',
        html: true
      });
      $.data($pin.get(0), 'effect', effect);
      $frame.append($pin);
    });

    // assign events
    $mapFrame.off('click.map');
    $mapFrame.on('click.map', function(e) {
      if ($(e.target).hasClass('pin')) {
        pinClicked($(e.target));
      } else {
        $frame.find('.pin.active').eq(0).trigger('click');
      }
    });
  };

  var layoutStepper = function () {
    var topicCount = 1;
    var currentSubTopic = 1;
    _(technique.topics).each(function(topic) {
      var $li = $(__el('li')).attr({'data-title': $('<div/>').html(topic.title).text(), 'data-id': topic.id}).append($(__el('a')).attr({'href': '#/' + topic.id, 'data-id': topic.id, 'title': topic.title})).addClass('topic');
      var subtopics = topic.subtopics;
      if ((subtopics != null) && (topic.id !== "recommendations")) {
        var subtopicCount = 1;
        var $subtopics = $(__el('ul'));
        _(subtopics).each(function(subtopic) {
          var $subtopic = $(__el('li')).addClass('subtopic');
          var $a = $(__el('a'));
          $a.attr({
            'href': '#/' + topic.id + '/' + subtopic.id,
            'data-id': subtopic.id,
            'title': topicCount + "\n" + subtopicCount + '/' + subtopics.length
          });
          if (subtopic.settings != null) {
            $a.attr('data-settings', subtopic.settings);
          }
          if (subtopic.featured != null) {
            $.data($a.get(0), 'featured', true);
          } else {
            $.data($a.get(0), 'featured', false);
          }
          $subtopic.append($a);
          $subtopics.append($subtopic);
          subtopicCount++;

          if (subtopic.mapInfo != null) {
            $.data($subtopic.get(0), 'mapInfo', subtopic.mapInfo);
          }

        });
        $li.append($subtopics);

      } else if (topic.id === 'recommendations') {
        lensRecommendations = topic;
      }
      $stepper.append($li);
      topicCount++;
    });
    $stepper.find('.subtopic > a').each(function (index, el) {
      $(el).attr('data-image-no', index + 1);
    });
    $stepper.on('click', '.subtopic > a', function(e) {
      var $this = $(this);
      var $subtopic = $(this).parent();
      var topicTitle = $(this).closest('.topic').attr('data-title');
      var topicID = $(this).closest('.topic').attr('data-id');
      var basePath = BASE_URL + technique.id +'/' + topicID + '/';
      var imagePath = basePath + $(this).attr('data-id') + '.jpg';

      var $img = $(__el('img')).attr('src', imagePath).css({width: 'auto', height: '100%'});
      // reset map
      $mapElements.find('.popover').remove();
      $mapElements.find('.pin').remove();
      $mapElements.find('.effect-overlay').remove();

      $img.on('load', function () {
        if ($.data($subtopic.get(0), 'mapInfo')) {
          layoutMapOnFrame($.data($subtopic.get(0), 'mapInfo'), $mapElements, basePath);
        } else {
          layoutMapOnFrame({effects: []}, $mapElements, basePath);
        }
        if ($.data($this.get(0), 'featured')) {
          $('.best-shot').show();
        } else {
          $('.best-shot').hide();
        }
        layoutCanvasAndImage($(this));
      });
      $imageFrame.html($img);

      var index = $(this).attr('data-image-no');
      $('.subtopic').removeClass('done');
      $('.topic > a').removeClass('done');
      $('.subtopic').slice(0, index).addClass('done');
      $('.subtopic.done').parent().siblings('a').addClass('done');

      $meta.html($(this).attr('data-settings'));
      $title.html(technique.title + ' / ' + $(this).closest('.topic').attr('data-title'));
      updateDisplayIndex();
      window.location.hash = $(this).attr('href');
      hideLensRecommendations();
      e.preventDefault();
    });

    $stepper.on('click', '.topic > a', function(e) {
      if ($(this).attr('data-id') === 'recommendations') {
        showLensRecommendations();
      } else {
        hideLensRecommendations();
        $(this).closest('.topic').find('.subtopic a:eq(0)').trigger('click');
      }
      e.preventDefault();
    });
    $('.subtopic > a, .topic > a').tooltip({placement: 'top'});
  };

  var layoutCanvasAndImage = function($img) {
    var calculateLeft = function(width) {
      return ($(window).width() - width) / 2;
    };
    var offset, availableHeight, availableWidth;
    if ($(window).height() < $(window).width()) {
      offset = 180;
      availableHeight = $(window).height() - offset;
      availableWidth = availableHeight * ($img[0].width / $img[0].height);
      $imageFrame.css({
        width: availableWidth,
        height: availableHeight,
        'margin-left': calculateLeft(availableWidth)
      });
      $mapFrame.css({
        width: availableWidth,
        height: availableHeight,
        left: calculateLeft(availableWidth),
        position: 'absolute',
        top: 0
      });
    } else {
      offset = 52 + 52;
      availableWidth = $(window).width();
      availableHeight = availableWidth * ($img[0].height / $img[0].width);
      $imageFrame.css({
        width: availableWidth,
        height: availableHeight,
        'margin-left': calculateLeft(availableWidth)
      });
      $mapFrame.css({
        width: availableWidth,
        height: availableHeight,
        left: 0,
        position: 'absolute',
        top: 0
      });
    }
    $imageFrame.find('img').eq(0).width(availableWidth).height(availableHeight);
  };
  var prevSubTopic = function () {
    var nextIndex = _.indexOf(jQuery('.subtopic'), jQuery('.subtopic.done').last().get(0)) - 1;
    if (nextIndex >= 0)
      $('.subtopic').eq(nextIndex).find('a').trigger('click');
  };
  var nextSubTopic = function () {
    var nextIndex = _.indexOf(jQuery('.subtopic'), jQuery('.subtopic.done').last().get(0)) + 1;
    if (nextIndex < $('.subtopic').length)
      $('.subtopic').eq(nextIndex).find('a').trigger('click');
    else
      showLensRecommendations();
  };
  var updateDisplayIndex = function () {
    var currentIndex = _.indexOf(jQuery('.subtopic'), jQuery('.subtopic.done').last().get(0));
    $('.index-display').html( currentIndex + 1 + ' | ' + $('.subtopic').length);
  };
  var showLensRecommendations = function () {
    // // if (! lensPopulated)
    //   populateLensRecommendations( lensRecommendations );
    // $('#lens-recommendations').show();
    $('html, body').animate({
        scrollTop: $('#lens-recommendations').offset().top
    }, 2000);
  };
  var hideLensRecommendations = function () {
    $('#shooting-recommendations').hide();
  }
  var populateLensRecommendations = function(lensTopic) {
    lensPopulated = true;
    var $recommendations = $('#shooting-recommendations');
    var categories = lensTopic.subtopics[0].categories;
    var th = __el('tr');
    var tr = __el('tr');
    _.each(categories, function (category, index, list)  {
      $(th).append($(__el('th')).html(category.name));
      var lenses = category.lenses;
      var trHTML = '';
      _.each(lenses, function (lens, index, list) {
        var lensURL = '../lens/' + lens.id.replace('-', '');
        var lensImageURL = '/util/lens.php?id=' + lens.id.replace('-', '');
        trHTML += '<div class="lens-container"><a href="' + lensURL + '"><img src="' + lensImageURL + '"><br>'+ lens.id.replace('-', '').toUpperCase() +'</a></div> ';
      });
      $(tr).addClass({borterTop: '1px solid #ccc'}).append($(__el('td')).html(trHTML));
    });
    $recommendations.find('table').append(th);
    $recommendations.find('table').append(tr);
  };

  // INIT STUFF
  layoutStepper();
  $('.next-subsection').on('click', function () {
    nextSubTopic();
  });
  $('.prev-subsection').on('click', function () {
    prevSubTopic();
  });
  $(window).on('resize', function (e) {
    $stepper.find('.subtopic').width($stepper.width() / ($stepper.find('.subtopic').length) - 3 );
    if ($imageFrame.find('img').length > 0)
      layoutCanvasAndImage($imageFrame.find('img'));
    else
      $imageFrame.height($(window).height() - 150);
  });
  $(document).keyup(function(e) {
    if (e.keyCode == 27) { // esc
      $('.pin.active').eq(0).trigger('click');
    }
    if (e.keyCode == 39) { // right arrow
      nextSubTopic();
    }
    if (e.keyCode == 37) { // left arrow
      prevSubTopic();
    }
  });
  $(window).trigger('resize');
  var hash = window.location.hash;
  if (hash === '') {
    $('.topic:eq(0) > a').trigger('click');
  } else {
    var hashComponents = hash.split('/');
    if (hashComponents.length === 3) {
      $('li.topic[data-id=' + hashComponents[1] + '] li.subtopic a[data-id="' + hashComponents[2] + '"]').trigger('click');
    }
  }
})(jQuery);