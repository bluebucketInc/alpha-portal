'use strict'

$(function() {

    Handlebars.registerHelper('learningArticlesIndex', function(number) {
        return parseInt(number) + 1;
    });

    Handlebars.registerHelper('learningArticlesRenameHTML', function(text) {
        // text = Handlebars.Utils.escapeExpression(text);

        return text.toLowerCase().replace(/[^\w\d\s]+/g, '').replace(/\ +/g, '-');
    });

    Handlebars.registerHelper('learningArticlesRenameImage', function(text) {
        // text = Handlebars.Utils.escapeExpression(text);

        return text.toLowerCase().replace(/[^\w\d\s]+/g, '').replace(/\ +/g, '_');
    });

    var section = '';

    $.ajax({
            url: 'data/learning_articles.json',
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            console.log("success");

            var containerTarget = $("#learning-container")[0];

            var containerObserver = new MutationObserver(function(mutations) {

                mutations.forEach(function(mutation) {
                    var newNodes = mutation.addedNodes; // DOM NodeList

                    if (newNodes !== null) { // If there are new nodes added
                        var $nodes = $(newNodes); // jQuery set

                        $nodes.each(function() {
                            var $node = $(this);

                            if ($node.hasClass('box-wrapper') && $node.hasClass('active-item')) {

                                $node.find('.thumb').load(function() {
                                    $('.learning-container .box-wrapper.active-item').standardHeight();
                                });
                            }
                        });
                    }
                });
            });

            var config = {
                childList: true
            };

            var navTarget = $(".filter-slider")[0];

            var navObserver = new MutationObserver(function(mutations) {
                var count = 0;

                mutations.forEach(function(mutation) {
                    var newNodes = mutation.addedNodes; // DOM NodeList

                    if (newNodes !== null) { // If there are new nodes added
                        var $nodes = $(newNodes); // jQuery set

                        if ($nodes.length > 0) {

                            $nodes.each(function() {
                                var $node = $(this);

                                if ($node.hasClass('slick-slide') && $node.hasClass('slick-active')) {
                                    count++;

                                    if (count >= Object.keys(data).length) {
                                        var active = $('.filter-slider').find('.filter-btn.active').data('section');
                                        active = parseInt(active.replace('section', ''));
                                        var index = /*Math.ceil(active / 3)*/ active - 1;

                                        $('.filter-slider').slick('slickGoTo', index);
                                    }
                                }
                            });
                        }
                    }
                });
            });

            containerObserver.observe(containerTarget, config);

            navObserver.observe(navTarget, config);

            try {
                var itemsTemplate = Handlebars.compile($('#items-template').html());
                var itemsHTML = stripStupidSpacesFrontAndBack(itemsTemplate(data));
                $('#items-template').replaceWith(itemsHTML);

                var itemsTitleTemplate = Handlebars.compile($('#itemsTitle-template').html());
                var itemsTitleHTML = stripStupidSpacesFrontAndBack(itemsTitleTemplate(data));
                $('#itemsTitle-template').replaceWith(itemsTitleHTML);

                var itemsNavTemplate = Handlebars.compile($('#itemsNav-template').html());
                var itemsNavHTML = stripStupidSpacesFrontAndBack(itemsNavTemplate(data));
                $('#itemsNav-template').replaceWith(itemsNavHTML);

            } catch (e) {
                console.log(e);
            }

            // Window width
            var windowWidth;

            var mobile = false;

            //-----------------------------------
            // Slick :: Second Nav
            //-----------------------------------
            $('.filter-slider').on('init', function(slick) {
                $('.filter-slider').animate({
                    opacity: 1
                })
            });

            $('.filter-slider').slick({
                slidesToShow: 5,
                slidesToScroll: 5,
                nextArrow: '<a class="slick-next" onclick="trackMs_link(\'ms:ilc::a7Series:accessories:rightArrow\', \'ilc:\', \'ilc::a7Series:accessories:rightArrow\', \'microsite|ilc:|a7Series|accessories|rightArrow\');"><img src="img/arrow_next.png" width="8" height="19" alt="next"></a>',
                prevArrow: '<a class="slick-prev" onclick="trackMs_link(\'ms:ilc::a7Series:accessories:leftArrow\', \'ilc:\', \'ilc::a7Series:accessories:leftArrow\', \'microsite|ilc:|a7Series|accessories|leftArrow\');"><img src="img/arrow_prev.png" width="8" height="19" alt="previous"></a>',
                responsive: [
                    // {
                    //  breakpoint: 668,
                    //  settings: {
                    //         slidesToShow: 5,
                    //         slidesToScroll: 5
                    //     }
                    // },
                    {
                        breakpoint: 568,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                                /*,
                                                            initialSlide: index*/
                        }
                    }
                ]
            });



            //-----------------------------------
            // Filter
            //-----------------------------------

            // CHECK HASH TAG
            checkHash();

            // HIGHLIGHT SELECTED
            $('.filter-section').find('.' + section).addClass('active');

            // BTN ON CLICK
            $('.filter-btn').on('click', function(e) {
                e.preventDefault();

                if ($(this).parent().hasClass('filter-section')) {
                    section = $(this).data('section');

                    var urlHash = '';

                    switch (section) {
                        case "section1":
                            if (typeof trackMs_link === 'function') {
                                trackMs_link('ms:aportal:articles:sec01:whatisacamera', '', 'aportal:articles:sec01:whatisacamera', 'microsite|aportal|articles|sec01|whatisacamera');
                            }
                            // console.log("trackMs_link('ms:aportal:articles:sec01:whatisacamera', '', 'aportal:articles:sec01:whatisacamera', 'microsite|aportal|articles|sec01|whatisacamera')");
                        default:
                            urlHash = 'section1';
                            break;
                        case "section2":
                            urlHash = 'section2';
                            if (typeof trackMs_link === 'function') {
                                trackMs_link('ms:aportal:articles:sec02:howtousecamera', '', 'aportal:articles:sec02:howtousecamera', 'microsite|aportal|articles|sec02|howtousecamera');
                            }
                            // console.log("trackMs_link('ms:aportal:articles:sec02:howtousecamera', '', 'aportal:articles:sec02:howtousecamera', 'microsite|aportal|articles|sec02|howtousecamera')");
                            break;
                        case "section3":
                            urlHash = 'section3';
                            if (typeof trackMs_link === 'function') {
                                trackMs_link('ms:aportal:articles:sec03:beforeyoushoot', '', 'aportal:articles:sec03:beforeyoushoot', 'microsite|aportal|articles|sec03|beforeyoushoot');
                            }
                            // console.log("trackMs_link('ms:aportal:articles:sec03:beforeyoushoot', '', 'aportal:articles:sec03:beforeyoushoot', 'microsite|aportal|articles|sec03|beforeyoushoot')");
                            break;
                        case "section4":
                            urlHash = 'section4';
                            if (typeof trackMs_link === 'function') {
                                trackMs_link('ms:aportal:articles:sec04:takinggreatppictures', '', 'aportal:articles:sec04:takinggreatppictures', 'microsite|aportal|articles|sec04|takinggreatppictures');
                            }
                            // console.log("trackMs_link('ms:aportal:articles:sec04:takinggreatppictures', '', 'aportal:articles:sec04:takinggreatppictures', 'microsite|aportal|articles|sec04|takinggreatppictures')");
                            break;
                        case "section5":
                            urlHash = 'section5';
                            if (typeof trackMs_link === 'function') {
                                trackMs_link('ms:aportal:articles:sec05:touchingup', '', 'aportal:articles:sec05:touchingup', 'microsite|aportal|articles|sec05|touchingup');
                            }
                            // console.log("trackMs_link('ms:aportal:articles:sec05:touchingup', '', 'aportal:articles:sec05:touchingup', 'microsite|aportal|articles|sec05|touchingup')");
                            break;
                    }

                    if (window.location.hash !== '#' + urlHash) {
                        window.location.hash = urlHash;

                        $('.filter-btn').removeClass('active');
                        $('h1').addClass('hidden');
                        $(this).addClass('active');
                        checkHash();

                        if (windowWidth < 769) {
                            $('.box-wrapper.active-item').pageMe({
                                pagerSelector: '#sectionPager'
                            });
                            // window.location.reload();
                        }

                        $('.learning-container .box-wrapper.active-item').standardHeight();

                    } else {
                        return;
                    }

                }

            });

            $(window).resize(function() {
                windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

                // console.log(windowWidth + ' | ' + mobile);

                if (windowWidth < 769 && mobile === false) {
                    $('.box-wrapper.active-item').pageMe({
                        pagerSelector: '#sectionPager'
                    });
                    mobile = true;

                } else if (windowWidth >= 769 && mobile === true) {
                    $('.learning-container .box-wrapper').show();
                    $('.learning-container #sectionPager').empty();
                    mobile = false;
                }
            });

            $(window).resize();
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");


        });



    //-----------------------------------
    // pagination
    //-----------------------------------

    $.fn.pageMe = function(opts) {
        var $this = this,
            defaults = {
                perPage: 6,
                showPrevNext: true
                    // numbersPerPage: 5,
                    // hidePageNumbers: false
            },
            settings = $.extend(defaults, opts);

        var listElement = $this;
        var perPage = settings.perPage;
        var children = listElement.length;
        var pager = $('.pagination');

        if (typeof settings.childSelector != "undefined") {
            children = listElement.find(settings.childSelector);
        }

        if (typeof settings.pagerSelector != "undefined") {
            pager = $(settings.pagerSelector);
        }

        var numItems = listElement.length;
        var numPages = Math.ceil(numItems / perPage);

        pager.data("curr", 0);

        if (settings.showPrevNext) {
            $('<div class="pager-prev"><a href="#" class="prev_link"><img src="../img/arrow_prev.png" width="4" height="10" alt="Previous"> <span>Previous</span></a></li>').appendTo(pager);
        }

        // var curr = 0;
        // while(numPages > curr && (settings.hidePageNumbers==false)){
        //     $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        //     curr++;
        // }

        // if (settings.numbersPerPage>1) {
        //    $('.page_link').hide();
        //    $('.page_link').slice(pager.data("curr"), settings.numbersPerPage).show();
        // }

        if (settings.showPrevNext) {
            $('<div class="pager-next"><a href="#" class="next_link"><span>Next</span> <img src="../img/arrow_next.png" width="4" height="10" alt="Next"></a></li>').appendTo(pager);
        }

        $('.pagination').show();
        // pager.find('.page_link:first').addClass('active');
        pager.find('.prev_link').hide();

        if (numPages <= 1) {
            pager.find('.next_link').hide();
            $('.pagination').hide();
        }
        // pager.children().eq(1).addClass("active");

        listElement.hide();
        listElement.slice(0, perPage).show();

        // pager.find('li .page_link').click(function(){
        //     var clickedPage = $(this).html().valueOf()-1;
        //     goTo(clickedPage,perPage);
        //     return false;
        // });
        pager.find('div .prev_link').click(function() {
            previous();
            return false;
        });
        pager.find('div .next_link').click(function() {
            next();
            return false;
        });

        var goToPage;

        function previous() {
            goToPage = parseInt(pager.data("curr")) - 1;
            goTo(goToPage);
        }

        function next() {
            goToPage = parseInt(pager.data("curr")) + 1;
            goTo(goToPage);
        }

        function goTo(page) {
            var startAt = page * perPage,
                endOn = startAt + perPage;

            listElement.css('display', 'none').slice(startAt, endOn).show();

            if (page >= 1) {
                pager.find('.prev_link').show();
            } else {
                pager.find('.prev_link').hide();
            }

            if (page < (numPages - 1)) {
                pager.find('.next_link').show();
            } else {
                pager.find('.next_link').hide();
            }

            pager.data("curr", page);

            //    if (settings.numbersPerPage>1) {
            //          $('.page_link').hide();
            //          $('.page_link').slice(page, settings.numbersPerPage+page).show();
            // }

            pager.children().removeClass("active");
            // pager.children().eq(page+1).addClass("active");

        }
    };



    /*if(windowWidth < 769){
        $('.box-wrapper.active-item').pageMe({pagerSelector:'#sectionPager'});
    }*/



    function checkHash() {
        $('.box-wrapper').addClass('hidden');

        var windowHash = window.location.hash;
        if (window.location.hash) {

            $('.box-wrapper').removeClass('active-item');
            $('.pagination div').remove();

            switch (windowHash) {
                case "#section1":
                default:
                    section = 'section1';
                    $('.box-wrapper.section1').removeClass('hidden');
                    $('.section-title .section1').removeClass('hidden');
                    $('.box-wrapper.section1').addClass('active-item');
                    break;
                case "#section2":
                    section = 'section2';
                    $('.box-wrapper.section2').removeClass('hidden');
                    $('.section-title .section2').removeClass('hidden');
                    $('.box-wrapper.section2').addClass('active-item');
                    break;
                case "#section3":
                    section = 'section3';
                    $('.box-wrapper.section3').removeClass('hidden');
                    $('.section-title .section3').removeClass('hidden');
                    $('.box-wrapper.section3').addClass('active-item');
                    break;
                case "#section4":
                    section = 'section4';
                    $('.box-wrapper.section4').removeClass('hidden');
                    $('.section-title .section4').removeClass('hidden');
                    $('.box-wrapper.section4').addClass('active-item');
                    break;
                case "#section5":
                    section = 'section5';
                    $('.box-wrapper.section5').removeClass('hidden');
                    $('.section-title .section5').removeClass('hidden');
                    $('.box-wrapper.section5').addClass('active-item');
                    break;
            }

        } else {
            window.location.hash = 'section1';
            section = 'section1';
            $('.box-wrapper.section1').removeClass('hidden');
            $('.section-title .section1').removeClass('hidden');
            $('.box-wrapper.section1').addClass('active-item');
        }
    }



    //! like the function name has said
    function stripStupidSpacesFrontAndBack(str) {
        return str.replace(/^[\s\t\n\r\u200B]+|[\s\t\n\r\u200B]+$/g, '');
    }

});
