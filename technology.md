Technology
======================== 

# Adding New Cameras/ Lenses Article

## Image

Article Images and thumbnails is located at `img/technology/cameras` for Camera, and `img/technology/lenses` for Lenses.  


## Landing Page  


HTML is located at `technology/index.template.html`.  

### Data   

1. Data is located at `data/tech_articles.json`.
2. Data for Camera is top half, and lenses is bottom half.
3. Way of filling the data
	* type : "camera" / "lens"
	* cate : "cameras" / "lenses"
	* name : name of the article
	* excerpt : short description of the article
	* url : to be same naming as the html files you create at `technology/cameras/` or `technology/lenses/`

~~~~
{
    "type": "camera",
    "cate": "cameras",
    "name": "Exmor&trade; CMOS sensor",
    "excerpt": "Exmor&trade; CMOS sensor with ClearVid array allows you to enjoy richer colours, smear-free images, high-sensitivity, high-speed processing and",
    "url": "exmor-cmos-sensor"
}
~~~~

~~~~
{
	"type": "lens",
	"cate": "lenses",
	"name": "Direct Drive Super Sonic Wave Motor",
	"excerpt": "A new DDSSM system is used for precision positioning of the heavy focus group required for the full-frame format",
	"url": "ddssm"
}
~~~~



## Article Content
1. Camera article is located at `technology/cameras`, and lenses article is located at `technology/lenses`.
2. `less/technology.less.less` to edit CSS.
3. `js/technology.less.js` to edit JS.


