'use strict'

$(function() {

    $('#navbar > a.reviews').addClass('active');

    Handlebars.registerHelper('equals', function(val1, val2, options) {

        if (arguments.length < 3)
            throw new Error("Handlebars Helper equal needs 2 parameters");

        if (val1 !== val2) {
            return options.inverse(this);

        } else {
            return options.fn(this);
        }
    });

    Handlebars.registerHelper('reviewsRenameTracking', function(text) {
        // text = Handlebars.Utils.escapeExpression(text);
        // console.log(text);
        // console.log(text.toLowerCase().replace('&trade;', '').replace(/[^\w\d\s]+/g, '').replace(/\ +/g, ''));
        return text.toLowerCase().replace('&trade;', '').replace(/[^\w\d\s]+/g, '').replace(/\ +/g, '');
    });

    $.ajax({
            url: 'data/review_articles.json',
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            console.log("success");

            var target = $("#reviews-container")[0];

            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    var newNodes = mutation.addedNodes; // DOM NodeList

                    if (newNodes !== null) { // If there are new nodes added
                        var $nodes = $(newNodes); // jQuery set

                        $nodes.each(function() {
                            var $node = $(this);

                            if ($node.hasClass('box-wrapper')) {

                                $node.find('.thumb').load(function() {
                                    $('.reviews-container .box-wrapper').standardHeight();
                                    // $('.box-wrapper').height(tabulateTallestHeight('.box-wrapper') + $('.cta').height() + 25);
                                });
                            }
                        });
                    }
                });
            });

            var config = {
                attributes: true,
                childList: true,
                subtree: true
            };

            observer.observe(target, config);

            try {
                var itemsTemplate = Handlebars.compile($('#items-template').html());
                var itemsHTML = stripStupidSpacesFrontAndBack(itemsTemplate(data));
                $('#items-template').replaceWith(itemsHTML);


            } catch (e) {
                console.log(e);
            }


            // Set equal height for multiple elements
            $(window).load(function() {
                $('.reviews-container .box-wrapper').standardHeight();
            });


            //-----------------------------------
            // Filter
            //-----------------------------------

            // CHECK HASH TAG
            var category = '';
            checkHash();

            function checkHash() {
                if (window.location.hash) {

                    if (window.location.hash === '#cameras') {
                        category = 'camera';
                        $('.box-wrapper.camera').removeClass('hidden');
                        $('.box-wrapper.lens').addClass('hidden');

                    } else if (window.location.hash === '#lenses') {
                        category = 'lens';
                        $('.box-wrapper.lens').removeClass('hidden');
                        $('.box-wrapper.camera').addClass('hidden');

                    } else {
                        window.location.hash = 'cameras';
                        category = 'camera';
                        $('.box-wrapper.camera').removeClass('hidden');
                        $('.box-wrapper.lens').addClass('hidden');
                    }

                } else {
                    window.location.hash = 'cameras';
                    category = 'camera';
                    $('.box-wrapper.camera').removeClass('hidden');
                    $('.box-wrapper.lens').addClass('hidden');
                }
            }


            // HIGHLIGHT SELECTED
            $('.filter-category').find('.' + category).addClass('active');


            // BTN ON CLICK
            $('.filter-btn').on('click', function(e) {
                e.preventDefault();

                if ($(this).parent().hasClass('filter-category')) {
                    category = $(this).data('category');
                    var urlHash = '';

                    if (category === 'camera') {
                        urlHash = 'cameras';

                        if (typeof trackMs_link === 'function') {
                            trackMs_link('ms:aportal:reviews:cameras', '', 'aportal:reviews:cameras', 'microsite|aportal|reviews|cameras');
                        }

                        // console.log("trackMs_link('ms:aportal:reviews:cameras', '', 'aportal:reviews:cameras', 'microsite|aportal|reviews|cameras')");

                    } else if (category === 'lens') {
                        urlHash = 'lenses';

                        if (typeof trackMs_link === 'function') {
                            trackMs_link('ms:aportal:reviews:lenses', '', 'aportal:reviews:lenses', 'microsite|aportal|reviews|lenses');
                        }

                        // console.log("trackMs_link('ms:aportal:reviews:lenses', '', 'aportal:reviews:lenses', 'microsite|aportal|reviews|lenses')");
                    }

                    if (window.location.hash !== '#' + urlHash) {
                        window.location.hash = urlHash;

                        $('.filter-btn').removeClass('active');
                        $(this).addClass('active');
                        checkHash();

                        // $('.reviews-container .box-wrapper.active-item').standardHeight();

                    } else {
                        return;
                    }

                }


            });


            $(window).resize();
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });


    //! like the function name has said
    function stripStupidSpacesFrontAndBack(str) {
        return str.replace(/^[\s\t\n\r\u200B]+|[\s\t\n\r\u200B]+$/g, '');
    }





});
