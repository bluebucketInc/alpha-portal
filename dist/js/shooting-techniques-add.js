$(function() {

  $('.models-slider').on('init', function() {
      $('.models-slider').animate({opacity:1});
  });

  $('.models-slider').slick({
    // autoplay: true,
    arrows: true,
    dots: false,
    accessibility: true,
    draggable: false,
    // swipe: true,
    slidesToShow: 9,
    slidesToScroll: 9,
    centerPadding: '20px',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
              slidesToShow: 6,
              slidesToScroll: 6
          }
      },
      {
        breakpoint: 613,
        settings: {
              slidesToShow: 4,
              slidesToScroll: 4
          }
      },
      {
        breakpoint: 468,
        settings: {
              slidesToShow: 3,
              slidesToScroll: 3
          }
      }
    ]
  });

  if (/iPad|iPhone|iPod/.test(navigator.platform)) {
      $("div.model-item").on('click', function() {
          window.open($(this).attr('href'));
      });
  } else {

    $("div.box-cta").attr('href', function(i, val) {
        return val + "#page-main-content";
    });

      $("div.model-item").colorbox({
          iframe: true,
          width: "95%",
          height: "95%",
          onComplete: setIframe
      });
  }

  $('div.model-item img').on('click', function () {
      var model = $(this).parents(".img-wrapper").siblings("p").text();
      trackMs_link('aportal:shooting:'+shootTechnique+':lenses:'+model, 'aportal', 'aportal:shooting:'+shootTechnique+':lenses:'+model, 'microsite|aportal|shooting|'+shootTechnique+'|lenses|'+shootTechnique);
  });

  function setIframe() {
          if (window.location.host == "sony-asia.com") {
              var cssLink = document.createElement("link");
              // to change css
              cssLink.href = "http://www.sony-asia.com/microsite/ilc/css/iframeoverwrite.css";
              cssLink.rel = "stylesheet";
              cssLink.type = "text/css";
              frames[0].onload = function() {
                  frames[0].document.head.appendChild(cssLink);
              };
          }
      }
});
