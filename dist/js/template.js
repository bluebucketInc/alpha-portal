$(function() {


    //-----------------------------------
    // Copyright Year
    //-----------------------------------
    copyrightYear();

    function copyrightYear() {
        $("#year").html(new Date().getFullYear());
    }


    //-----------------------------------
    // Sticky Menu bar
    //-----------------------------------

    // Window width
    var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)

    if (windowWidth > 1023) {
        $(window).on('scroll', function() {
            $('.secondary-nav').toggleClass('fixed', $(window).scrollTop() > 114);
        });
    }


    //-----------------------------------
    // Navbar maxheight for small screen
    //-----------------------------------

    var windowHeight = window.innerHeight;

    if ((windowHeight >= 319) && (windowHeight < 400)) {
        $('.navbar-fixed-bottom .navbar-collapse, .navbar-fixed-top .navbar-collapse').addClass("navbar-maxheight")
    }

    //-----------------------------------
    // Secondary Nav :: Mobile
    //-----------------------------------
    mobileSecNavInit();

    function mobileSecNavInit() {
        $('#filter-content-wrapper').slideDown(400, function() {
            $('#filter-text .up, #filter-text .down').toggle();
            $('#filter-content-wrapper').delay(600).slideUp(400, function() {
                $('#filter-text .up, #filter-text .down').toggle();
                enableMobileNav();
            });
        });
    }

    function enableMobileNav() {
        $('#filter-text').click(function() {
            if ($(this).hasClass('filter-opened')) {
                $('#filter-content-wrapper').slideUp();
                $(this).removeClass('filter-opened');
                $(this).children(".up, .down").toggle();
            } else {
                $('#filter-content-wrapper').slideDown();
                $(this).addClass('filter-opened')
                $(this).children(".up, .down").toggle();
            }

        })
    }



    //-----------------------------------
    // Back to top
    //-----------------------------------

    var offset = 100, // browser window scroll (in pixels) after which the "back to top" link is shown
        scroll_top_duration = 700, //duration of the top scrolling animation (in ms)
        $back_to_top = $('.scroll_to_top');

    //hide or show the "back to top" link
    $(window).scroll(function() {
        ($(this).scrollTop() > offset) ? $back_to_top.addClass('top-is-visible'): $back_to_top.removeClass('top-is-visible');
        // ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
    });

    //smooth scroll to top
    $back_to_top.on('click', function(event) {
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0,
        }, scroll_top_duration);

        //tracking
        if (typeof trackMs_link === 'function') {
            trackMs_link('ms:aportal:backtotop', '', 'aportal:backtotop', 'microsite|aportal|backtotop');
        }
    });


    //----------------------------------------
    // Set equal height for multiple elements
    //----------------------------------------

    $.fn.standardHeight = function() {
        var target = this;
        init();

        $(window).resize(function() {
            init();
        })

        function init() {

            target.height("auto");
            var divHeightArray = [];
            var totalDivs = target.length;

            target.each(function() {
                var t = $(this).height();
                divHeightArray.push(t);

                var arrayLength = divHeightArray.length;

                if (arrayLength == totalDivs) {
                    var maxValueInArray = Math.max.apply(Math, divHeightArray);
                    target.height(maxValueInArray);
                }
            })

        }
    }

    $(window).load(function() {
        // $('.tech-container .box-wrapper').standardHeight();
        // $('.learning-container .box-wrapper').standardHeight();
        $('.playmemories-app-container .box-wrapper .box-container').standardHeight();
    })

});
