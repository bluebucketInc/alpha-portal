$(function() {

    $('#navbar > a.products').addClass('active');

    Handlebars.registerHelper('toLowerCase', function(text) {
        text = Handlebars.Utils.escapeExpression(text);

        return text.toLowerCase().replace(' ', '_');
    });

    Handlebars.registerHelper('lastItemCheck', function(index, length, options) {

        if (arguments.length < 3) {
            throw new Error("Handlebars Helper equal needs 2 parameters");
        }

        if (index === (length - 1)) {
            return options.fn(this);

        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('mod', function(val1, val2, result, options) {

        if (arguments.length < 4) {
            throw new Error("Handlebars Helper equal needs 3 parameters");
        }

        if (val1 % val2 === result) {
            return options.fn(this);

        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('equals', function(val1, val2, options) {

        if (arguments.length < 3)
            throw new Error("Handlebars Helper equal needs 2 parameters");

        if (val1 !== val2) {
            return options.inverse(this);

        } else {
            return options.fn(this);
        }
    });

    //! determine if it is cameras or lenses page
    var category = '';

    if (window.location.hash) {

        if (window.location.hash === '#cameras') {
            category = 'camera';
            $('.filter-type').addClass('hidden');

        } else if (window.location.hash === '#lenses') {
            category = 'lens';
            $('.filter-type').removeClass('hidden');

        } else {
            window.location.hash = 'cameras';
            category = 'camera';
            $('.filter-type').addClass('hidden');
        }

    } else {
        window.location.hash = 'cameras';
        category = 'camera';
        $('.filter-type').addClass('hidden');
    }

    $('.filter-category').find('.' + category).addClass('active');

    //! initialise var
    //! data for filtering
    var mount = '';
    var sensor = '';
    var type = [];

    var column = getColumn();
    var products;

    //! load the data json file
    $.ajax({
        url: 'data/products.json',
        type: 'GET',
        dataType: 'json'
    })

    .done(function(data) {
            console.log("success");

            //! filter out the category
            //! camera OR lens
            products = _.filter(data, function(n) {
                return n.category.toLowerCase() === category;
            });

            // templatingHTML(products);

            $('.filter-btn').on('click', function(e) {
                e.preventDefault();

                //! clear filters
                if ($(this).hasClass('clear')) {
                    $('.filter-btn').removeClass('active');
                    $('.filter-category').find('[data-category="' + category + '"]').addClass('active');

                    mount = '';
                    sensor = '';
                    type = [];

                    $('.filter-content-wrapper select').val(null);

                    return filter(products, category, mount, sensor, type);
                }

                //! filter category
                if ($(this).parent().hasClass('filter-category')) {
                    category = $(this).data('category');
                    var urlHash = '';

                    if (category === 'camera') {
                        urlHash = 'cameras';
                        $('.filter-type').addClass('hidden');

                        if (typeof trackMs_link === 'function') {
                            trackMs_link('ms:aportal:products:cameras', '', 'aportal:products:cameras', 'microsite|aportal|products|cameras');
                        }

                        // console.log("trackMs_link('ms:aportal:products:cameras', '', 'aportal:products:cameras', 'microsite|aportal|products|cameras')");

                    } else if (category === 'lens') {
                        urlHash = 'lenses';
                        $('.filter-type').removeClass('hidden');

                        if (typeof trackMs_link === 'function') {
                            trackMs_link('ms:aportal:products:lenses', '', 'aportal:products:lenses', 'microsite|aportal|products|lenses');
                        }

                        // console.log("trackMs_link('ms:aportal:products:lenses', '', 'aportal:products:lenses', 'microsite|aportal|products|lenses')");
                    }

                    if (window.location.hash !== '#' + urlHash) {
                        window.location.hash = urlHash;

                        $('.filter-btn').removeClass('active');
                        $(this).addClass('active');

                        mount = '';
                        sensor = '';
                        type = [];

                        products = _.filter(data, function(n) {
                            return n.category.toLowerCase() === category;
                        });

                        $('.filter-content-wrapper select').val(null);

                        return filter(products, category, mount, sensor, type);

                    } else {
                        return;
                    }
                }

                //! filter behaviour for type
                if ($(this).parent().hasClass('filter-type')) {
                    $(this).toggleClass('active');

                    if ($(this).hasClass('active')) {
                        type.push($(this).data('type'));

                    } else {
                        type.splice(type.indexOf($(this).data('type')), 1);
                    }

                    type = _.uniq(type);

                } else {

                    $(this).toggleClass('active');
                    $(this).siblings().removeClass('active');

                    //! store filter data for camera and sensor
                    if (typeof $(this).data('mount') !== 'undefined') {
                        mount = ($(this).hasClass('active')) ? $(this).data('mount') : null;

                    } else if (typeof $(this).data('sensor') !== 'undefined') {
                        sensor = ($(this).hasClass('active')) ? $(this).data('sensor') : null;
                    }

                    // console.log('mount: ' + mount);
                    // console.log('sensor: ' + sensor);

                    // $(this).addClass('active');
                }

                return filter(products, category, mount, sensor, type);
            });

            $('.filter-content-wrapper').on('change', 'select', function(e) {

                if ($(this).is('#filter-mount')) {
                    // console.log($('.filter-btn[data-mount=' + $(this).val() + ']'));
                    $('.filter-btn[data-mount=' + $(this).val() + ']').trigger('click');

                } else if ($(this).is('#filter-sensor')) {
                    // console.log($('.filter-btn[data-sensor=' + $(this).val() + ']'));
                    $('.filter-btn[data-sensor=' + $(this).val() + ']').trigger('click');

                } else if ($(this).is('#filter-type')) {
                    $('.filter-type .filter-btn.active').trigger('click');
                    $('.filter-btn[data-type=' + $(this).val() + ']').trigger('click');
                }
            });

            $('.products-container').on('click', '.cta', function(e) {
                e.preventDefault();

                var target = $(this).attr('href');
                var content = $(this).siblings(target).html();
                var contentPlaceholder = $(this).closest('.box-wrapper').nextAll('.content-placeholder.' + column);
                // console.log(contentPlaceholder.first());

                if ($(this).hasClass('active')) {
                    $(this).removeClass('active').blur();
                    contentPlaceholder.first().stop().slideUp();

                } else {
                    $('.products-container').find('.cta').removeClass('active');
                    $(this).addClass('active').blur();

                    var scrollTarget = $(this).closest('.box-wrapper');

                    $('.products-container').find('.content-placeholder').stop().slideUp(function() {
                        // $('a.digital-imaging-compatibility').colorbox.remove();
                        // $(this).find('.content').html(content);
                        // console.log($(this).find('.content'));
                    }).promise().done(function() {
                        contentPlaceholder.first().find('.content').html(content);

                        contentPlaceholder.first().find('.digital-imaging-compatibility, .box-cta').colorbox({
                            iframe: true,
                            width: '95%',
                            height: '95%',
                            onComplete: setIframe
                        });

                        var slick = contentPlaceholder.first().find('.explore-box').parent();

                        slick.on('init', function(e, slick) {
                            $(window).resize();
                        })

                        if ($(window).width() <= 991) {
                            slickOptions = {
                                accessibility: false,
                                arrows: false,
                                dots: true,
                                rows: 2,
                                slidesPerRow: 2
                            };

                        } else {
                            slickOptions = {
                                accessibility: false,
                                arrows: false,
                                dots: true,
                                slidesToShow: 5
                            };
                        }

                        slick.slick(slickOptions);
                        // slick.slick('setPosition');

                        contentPlaceholder.first().slideDown(function() {
                            $('html, body').stop().animate({
                                scrollTop: $(this).prevAll('.box-wrapper').first().offset().top - 125
                            });
                        });
                    });

                    contentPlaceholder.find('.indicator').css({
                        left: scrollTarget.offset().left + (scrollTarget.width() / 2) - $('.products-container').offset().left
                    });

                }

            });

        })
        .fail(function() {
            console.log("error");
        })

    .always(function() {
        console.log("complete");

        var target = $("#products-container")[0];

        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                var newNodes = mutation.addedNodes; // DOM NodeList

                if (newNodes !== null) { // If there are new nodes added
                    var $nodes = $(newNodes); // jQuery set

                    $nodes.each(function() {
                        var $node = $(this);

                        if ($node.hasClass('box-wrapper')) {

                            $node.find('.thumb').load(function() {
                                $('.box-wrapper').height(tabulateTallestHeight('.box-wrapper') + $('.cta').height() + 25);
                                $(".explore-box .title").height(tabulateTallestHeight('.explore-box .title') + 25);
                                // console.log('MutationObserver');
                            });
                        }
                    });
                }
            });
        });

        var config = {
            attributes: true,
            childList: true,
            subtree: true
        };

        observer.observe(target, config);

        templatingHTML(products);

        $(window).resize(function() {
            var newColumn = getColumn();

            if (column === newColumn) {
                return;
            }

            column = newColumn;

            var openCta = $('.products-container').find('.cta.active');
            // console.log(openCta.length);

            if (openCta.length > 0) {
                openCta.trigger('click');

                // updateOffset();

                $('.box-wrapper').height(tabulateTallestHeight('.box-wrapper') + $('.cta').height() + 25);

                openCta.trigger('click');
            }
        });

        /*window.onhashchange = function() {
            console.log('asfn');
            // console.log($('.box-wrapper').height());
            // console.log($('.box-wrapper').find('.img-wrapper img'));
            $('.box-wrapper').find('.img-wrapper img').load(function() {
                console.log($(this).closest('.box-wrapper').height());
                $('.box-wrapper').height(tabulateTallestHeight('.box-wrapper') + $('.cta').height() + 25);
            });
            // $(window).trigger('load');
            // $('.box-wrapper').height(tabulateTallestHeight('.box-wrapper') + $('.cta').height() + 25);
        }*/

        /*$(window).load(function() {
            // updateOffset();
            // console.log($('.box-wrapper').height());
            $('.box-wrapper').height(tabulateTallestHeight('.box-wrapper') + $('.cta').height() + 25);
        });*/
    });

    function filter(data, category, mount, sensor, type) {
        var result = _.filter(data, function(n) {
            return (_.isEmpty(category) || n.category === category) && (_.isEmpty(mount) || n.mount === mount) && (_.isEmpty(sensor) || n.sensor === sensor) && (_.isEmpty(type) || _.every(type, function(_type) {
                return _.includes(n.type, _type);
            }));
        });

        templatingHTML(result);
    }

    function templatingHTML(data) {

        try {
            var productsTemplate = Handlebars.compile($('#products-template').html());
            var productsHTML = stripStupidSpacesFrontAndBack(productsTemplate(data));
            // var productsHTML = productsTemplate(products);
            // $('#products-template').replaceWith(productsHTML);
            // $('.box-wrapper').remove();
            $('#products-template').siblings().remove();
            $('#products-template').after(productsHTML);

            if (/iPad|iPhone|iPod/.test(navigator.platform)) {

                $("div.box-cta").on('click', function() {
                    window.open($(this).attr('href'));
                });

            } else {

                /*$("div.box-cta").attr('href', function(i, val) {
                    // console.log(val);
                    // return val + "#page-main-content";
                });*/

                $('div.box-cta').colorbox({
                    iframe: true,
                    width: '95%',
                    height: '95%',
                    onComplete: setIframe
                });

            }

        } catch (e) {
            console.log(e);
        }
    }

    function getColumn() {
        if ($(window).width() >= 1200) {
            return 'four-column';

        } else if ($(window).width() >= 992) {
            return 'three-column';

        } else {
            return 'two-column';
        }
    }

    function updateOffset() {
        var offset = 125;

        // _.delay(function() {
        $('.box-wrapper').each(function() {
            // console.log($(this).height() + ' VS ' + $(this).find('.box-container').height());
            // console.log('updateOffset');
            $(this).data('offset', $(this).offset().top - offset);
        });

        // console.log('tabulateTallestHeight');
        $('.box-wrapper').height(tabulateTallestHeight('.box-wrapper') + $('.cta').height() + 25);

        // }, 150);
    }

    function tabulateTallestHeight(target) {
        var tallest = 0;
        $(target).height('auto');

        $(target).each(function() {
            if ($(this).height() > tallest) {
                tallest = $(this).height();
            }
        });

        // console.log(tallest);

        return tallest;
    }

    //! like the function name has said
    function stripStupidSpacesFrontAndBack(str) {
        return str.replace(/^[\s\t\n\r\u200B]+|[\s\t\n\r\u200B]+$/g, '');
    }

    function setIframe() {
        if (window.location.host == "sony-asia.com") {
            var cssLink = document.createElement("link");
            // to change css
            cssLink.href = "http://www.sony-asia.com/microsite/ilc/css/iframeoverwrite.css";
            cssLink.rel = "stylesheet";
            cssLink.type = "text/css";
            frames[0].onload = function() {
                frames[0].document.head.appendChild(cssLink);
            };
        }
    }

});
