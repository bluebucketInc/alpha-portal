'use strict'

$(function() {

    $('#navbar > a.playmemories').addClass('active');

    Handlebars.registerHelper('playmemoriesRename', function(text) {
        // text = Handlebars.Utils.escapeExpression(text);

        // return text.toLowerCase().replace(/[^\w\d\s]+/g, '').replace(/\ +/g, '_');
        return text.toLowerCase().replace(/\++/g, '').replace(/\ +/g, '_');
    });

    Handlebars.registerHelper('onClickRename', function(text) {;
        return text.toLowerCase().replace(/\++/g, '').replace(/\ +/g, '').replace(/\-/g, '');
    });

    $.ajax({
            url: 'data/playmemories.json',
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            console.log("success");

            try {
                var itemsTemplate = Handlebars.compile($('#items-template').html());
                var itemsHTML = stripStupidSpacesFrontAndBack(itemsTemplate(data));
                $('#items-template').replaceWith(itemsHTML);

            } catch (e) {
                console.log(e);
            }

            // Window width
            var windowWidth;

            var mobile = false;

            $(window).resize(function() {
            	windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

                // console.log(windowWidth + ' | ' + mobile);

                if (windowWidth < 769 && mobile === false) {
                    $('.playmemories-app-container .box-wrapper').pageMe({
                        pagerSelector: '#sectionPager'
                    });
                    mobile = true;

                } else if (windowWidth >= 769 && mobile === true) {
                    $('.playmemories-app-container .box-wrapper').show();
                    $('.playmemories-app-container #sectionPager').empty();
                    mobile = false;
                }
            });

            $(window).resize();
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });



    //-----------------------------------
    // pagination
    //-----------------------------------

    $.fn.pageMe = function(opts) {
        var $this = this,
            defaults = {
                perPage: 6,
                showPrevNext: true
                    // numbersPerPage: 5,
                    // hidePageNumbers: false
            },
            settings = $.extend(defaults, opts);

        var listElement = $this;
        var perPage = settings.perPage;
        var children = listElement.length;
        var pager = $('.pagination');

        if (typeof settings.childSelector != "undefined") {
            children = listElement.find(settings.childSelector);
        }

        if (typeof settings.pagerSelector != "undefined") {
            pager = $(settings.pagerSelector);
        }

        var numItems = listElement.length;
        var numPages = Math.ceil(numItems / perPage);

        pager.data("curr", 0);

        if (settings.showPrevNext) {
            $('<div class="pager-prev"><a href="#" class="prev_link"><img src="img/arrow_prev.png" width="4" height="10" alt="Previous"> <span>Previous</span></a></li>').appendTo(pager);
        }

        // var curr = 0;
        // while(numPages > curr && (settings.hidePageNumbers==false)){
        //     $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        //     curr++;
        // }

        // if (settings.numbersPerPage>1) {
        //    $('.page_link').hide();
        //    $('.page_link').slice(pager.data("curr"), settings.numbersPerPage).show();
        // }

        if (settings.showPrevNext) {
            $('<div class="pager-next"><a href="#" class="next_link"><span>Next</span> <img src="img/arrow_next.png" width="4" height="10" alt="Next"></a></li>').appendTo(pager);
        }

        $('.pagination').show();
        // pager.find('.page_link:first').addClass('active');
        pager.find('.prev_link').hide();

        if (numPages <= 1) {
            pager.find('.next_link').hide();
            $('.pagination').hide();
        }
        // pager.children().eq(1).addClass("active");

        listElement.hide();
        listElement.slice(0, perPage).show();

        // pager.find('li .page_link').click(function(){
        //     var clickedPage = $(this).html().valueOf()-1;
        //     goTo(clickedPage,perPage);
        //     return false;
        // });

        pager.find('div .prev_link').click(function() {
            previous();
            return false;
        });
        pager.find('div .next_link').click(function() {
            next();
            return false;
        });

        var goToPage;

        function previous() {
            goToPage = parseInt(pager.data("curr")) - 1;
            goTo(goToPage);
        }

        function next() {
            goToPage = parseInt(pager.data("curr")) + 1;
            goTo(goToPage);
        }

        function goTo(page) {
            var startAt = page * perPage,
                endOn = startAt + perPage;

            listElement.css('display', 'none').slice(startAt, endOn).show();

            if (page >= 1) {
                pager.find('.prev_link').show();
            } else {
                pager.find('.prev_link').hide();
            }

            if (page < (numPages - 1)) {
                pager.find('.next_link').show();
            } else {
                pager.find('.next_link').hide();
            }

            pager.data("curr", page);

            //    if (settings.numbersPerPage>1) {
            //   		$('.page_link').hide();
            //   		$('.page_link').slice(page, settings.numbersPerPage+page).show();
            // }

            pager.children().removeClass("active");
            // pager.children().eq(page+1).addClass("active");

        }
    };

    //! like the function name has said
    function stripStupidSpacesFrontAndBack(str) {
        return str.replace(/^[\s\t\n\r\u200B]+|[\s\t\n\r\u200B]+$/g, '');
    }

});
