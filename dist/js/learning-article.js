$(function() {

    //-----------------------------------
    // Slick :: Second Nav
    //-----------------------------------
    $('.filter-slider').on('init', function() {
        $('.filter-slider').animate({
            opacity: 1
        });
    });

    $('.filter-slider').slick({
        slidesToShow: 5,
        slidesToScroll: 5,
        nextArrow: '<a class="slick-next" onclick="trackMs_link(\'ms:ilc::a7Series:accessories:rightArrow\', \'ilc:\', \'ilc::a7Series:accessories:rightArrow\', \'microsite|ilc:|a7Series|accessories|rightArrow\');"><img src="img/arrow_next.png" width="8" height="19" alt="next"></a>',
        prevArrow: '<a class="slick-prev" onclick="trackMs_link(\'ms:ilc::a7Series:accessories:leftArrow\', \'ilc:\', \'ilc::a7Series:accessories:leftArrow\', \'microsite|ilc:|a7Series|accessories|leftArrow\');"><img src="img/arrow_prev.png" width="8" height="19" alt="previous"></a>',
        responsive: [
            // {
            //  breakpoint: 668,
            //  settings: {
            //         slidesToShow: 5,
            //         slidesToScroll: 5
            //     }
            // },
            {
                breakpoint: 568,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            }
        ]
    });

    var active = $('.filter-slider').find('.filter-btn.active').data('section');
    active = parseInt(active.replace('section', ''));
    var index = /*Math.ceil(active / 3)*/ active - 1;

    $('.filter-slider').slick('slickGoTo', index);

});
